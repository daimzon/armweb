#!/usr/bin/env bash
VERSION="1.0.18-prod"
echo ${VERSION}
docker build -t awp_angular:latest .
docker tag  awp_angular:latest gitlab-registry.kazincombank.kz:30444/bekbaganbetov_a/awp/awp_angular:${VERSION}
docker push gitlab-registry.kazincombank.kz:30444/bekbaganbetov_a/awp/awp_angular:${VERSION}