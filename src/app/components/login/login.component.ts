import { Component, OnInit } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import {clientService} from '../../services/client.service'
import { ToastrService } from 'ngx-toastr';
import { MatSnackBar } from '@angular/material/snack-bar';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  firstFormGroup: FormGroup;
  username: any;
  password: any;

  constructor(
      public router: Router,
      public usrService: clientService,
        private toastr: ToastrService,
        private snackBar: MatSnackBar,
        private _formBuilder: FormBuilder,

  ) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      login: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
            duration: 2000,
        });
    }

  showError() {
    this.toastr.error('Неверный логин или пароль');
  }


  login() {
    this.usrService.auth(this.firstFormGroup.controls['login'].value, this.firstFormGroup.controls['password'].value)
  }

  goToRequest(id) {

  }

}
