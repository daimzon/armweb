import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-new-password',
    templateUrl: './new-password.component.html',
    styleUrls: ['./new-password.component.scss']
})
export class NewPasswordComponent implements OnInit {

    constructor() { }

    password: string = '';
    rePassword: string = '';
    disablePass: boolean = false;

    ngOnInit() {
    }

    checkPassword(){
        let strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
        if (strongRegex.test(this.password) && this.password === this.rePassword) {
            this.disablePass = true;
        }
    }

}
