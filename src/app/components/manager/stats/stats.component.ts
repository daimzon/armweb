import { Component, OnInit } from '@angular/core';
import {requestService} from "../../../services/request-list.service";

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit {
  public fourChartType: string = 'line';

  public fourChartDatasets: Array<any> = [
    { data: [65000000, 59000000, 80000000, 81000000, 56000000, 55000000, 40000000], label: 'Залоговые' },
    { data: [28000000, 48000000, 40000000, 19000000, 86000000, 27000000, 90000000], label: 'Беззалоговые' }
  ];

  public fourChartLabels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];

  public fourChartColors: Array<any> = [
    {
      backgroundColor: 'rgba(105, 0, 132, .2)',
      borderColor: 'rgba(200, 99, 132, .7)',
      borderWidth: 2,
    },
    {
      backgroundColor: 'rgba(0, 137, 132, .2)',
      borderColor: 'rgba(0, 10, 130, .7)',
      borderWidth: 2,
    }
  ];

  public fourChartOptions: any = {
    responsive: true
  };
  public fourChartClicked(e: any): void { }
  public fourChartHovered(e: any): void { }

  public chartType: string = 'bar';
  public secondChartType: string = 'doughnut';
  public chartDatasets: Array<any> = [
    { data: [500000, 100000, 1500000, 2000000, 3000000, 6000000, 1000000], label: 'My First dataset' }
  ];

  public chartLabels: Array<any> = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь'];

  public chartColors: Array<any> = [
    {
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true
  };
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }
  public secondChartDatasets: Array<any>
  public secondChartLabels: Array<any> = ['Одобрено', 'Отказ', 'Черновик'];
  public secondChartColors: Array<any> = [
    {
      backgroundColor: ['#F7464A', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'],
      hoverBackgroundColor: ['#FF5A5E', '#5AD3D1', '#FFC870', '#A8B3C5', '#616774'],
      borderWidth: 2,
    }
  ];

  public secondChartOptions: any = {
    responsive: true
  };
  public secondChartClicked(e: any): void { }
  public secondChartHovered(e: any): void { }

  requests: any;

  constructor() { }

  ngOnInit() {
    this.last()
  }

  last() {

  this.secondChartDatasets = [
      { data: [10, 20, 5], label: 'Статистика по месяцам' }
    ];

  }

  now() {
    this.secondChartDatasets = [
      { data: [2, 3, 5], label: 'Статистика по месяцам' }
    ];
  }

}
