import { Component, OnInit } from '@angular/core';
import {requestService} from '../../../services/request-list.service';
import {ActivatedRoute, Router} from '@angular/router';
import {LocalStorage} from '@ngx-pwa/local-storage';
import {MatDialog} from '@angular/material';
import {catalogService} from '../../../services/catalogs.service';
import {ToastrService} from 'ngx-toastr';
import {RiskService} from '../../../services/risk.service';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
import * as moment from 'moment';
import {saveAs as importedSaveAs} from 'file-saver';

@Component({
  selector: 'app-risk-list',
  templateUrl: './risk-list.component.html',
  styleUrls: ['./risk-list.component.scss']
})
export class RiskListComponent implements OnInit {
  public date = {begin: new Date(), end: new Date()};
  requests: any;
  filterType: any;
  creditType: any;
  statusType: any;
  searchType: any = '';
  searchStatus: any = '';
  userRole: any;
  requestField: any = '';
  fourSm = false;
  showNewButton = false;
  pageCount = 1;
  riskOfficer = false;
  dateRequest = {
    begin: '',
    end: ''
  };
  backAvailable: boolean;
  nextAvailable: boolean;

  constructor(
    public reqService: requestService,
    public router: Router,
    private route: ActivatedRoute,
    protected localStorage: LocalStorage,
    public dialog: MatDialog,
    public catalogs: catalogService,
    private toastr: ToastrService,
    private riskServ: RiskService,
    private spinnerService: Ng4LoadingSpinnerService,
  ) {
  }

  ngOnInit() {
    this.getCreditType();
    this.getStatusType();
    this.testStats();
    this.getUserRole();
    // this.update();
  }

  showSuccess(id) {
    this.toastr.success(id, 'Заявка инициализируется');
  }

  showUpdateOk(text) {
    this.toastr.success(text);
  }

  showUpdateFalse(text) {
    this.toastr.error(text);
  }

  getCreditType() {
    const type = 'producttype';
    this.catalogs.getCatalog(type).subscribe(
      (data: any) => {
        this.creditType = data;
      },
    );
  }


  getUserRole() {
    this.spinnerService.show();
    const role = localStorage.getItem('permissions');
    this.userRole = role.includes('LA');
    console.log('CHECK LIST:', this.userRole);
      this.getRiskList();
      this.riskOfficer = true;

    if (role.includes('SM04')) {
      this.fourSm = true;
    }

    if (role.includes('SM')) {
      this.showNewButton = true;
    }
  }

  getStatusType() {
    this.catalogs.getStatusList().subscribe(
      (data: any) => {
        this.statusType = data;
      }
    );
  }


  testStats() {
    this.catalogs.getLastMonthes().subscribe(
      (data: any) => {
        console.log(data);
      }
    );
  }

  getRiskList() {

    const params = {
      product_type: this.searchType,
      status: '',
      q: this.requestField,
      date_after: '',
      date_before: '',
      count: this.pageCount
    };

    this.riskServ.getRiskFullList(params).subscribe(
      (data: any) => {
        this.requests = data.results;
        this.nextAvailable = data.next;

        if (this.pageCount > 1) {
          this.backAvailable = true;
        } else {
          this.backAvailable = false;
        }
        this.requests = data.results;
        for (let i = 0; i < data.results.length; i++) {
          if (data.results[i].status === 1) {
            data.results[i].status_header = 'Ожидает заполнения';
          } else if (data.results[i].status === 2) {
            data.results[i].status_header = 'В процессе формирования согласия';
          } else if (data.results[i].status === 3) {
            data.results[i].status_header = 'Согласие сформировано';
          } else if (data.results[i].status === 4) {
            data.results[i].status_header = 'В работе';
          } else if (data.results[i].status === 5) {
            data.results[i].status_header = 'В работе риск офицера';
          } else if (data.results[i].status === 0) {
            data.results[i].status_header = 'Создано';
          } else if (data.results[i].status === 6) {
            data.results[i].status_header = 'Даны предложения';
          } else if (data.results[i].status === 7) {
            data.results[i].status_header = 'В процессе формирования договора';
          } else if (data.results[i].status === 8) {
            data.results[i].status_header = 'Договор сформирован';
          } else if (data.results[i].status === 9) {
            data.results[i].status_header = 'В работе кредитного администратора';
          } else if (data.results[i].status === 10) {
            data.results[i].status_header = 'Одобренно кредитным администратором';
          } else if (data.results[i].status === 11) {
            data.results[i].status_header = 'Редактируется риск офицером';
          } else if (data.results[i].status === 200) {
            data.results[i].status_header = 'Выдано';
          } else if (data.results[i].status === 500) {
            data.results[i].status_header = 'Отказано по техническим причинам';
          } else if (data.results[i].status === 501) {
            data.results[i].status_header = 'Отказано банком';
          } else if (data.results[i].status === 502) {
            data.results[i].status_header = 'Отказано клиентом';
          } else if (data.results[i].status === 503) {
            data.results[i].status_header = 'Заявка закрыта в связи открытии новой';
          } else if (data.results[i].status === 504) {
            data.results[i].status_header = 'Заявка закрыта в связи с бездействием';
          }
        }
        const text = 'Список обновлен';
        this.showUpdateOk(text);
      },
      (err: any) => {
        const text = 'Техническая ошибка';
        this.showUpdateFalse(text);
      }
    );
  }


  getStats() {
    this.reqService.getStats().subscribe(
      (data: any) => {
        console.log(data);
        importedSaveAs(data, 'report.xlsx');
      });
  }


  goToRequest(id, readable_id, status) {
    if (status === 1) {
      this.router.navigate(['manager/new-profile', id]);
    } else if (status === 0) {
      this.router.navigate(['manager/request-view', id]);
      this.showSuccess(readable_id);
    } else {
      this.router.navigate(['manager/request-view', id]);
    }
  }

  goToNewRequest() {
    this.router.navigate(['manager/new-request']);
  }

  nextPage() {
    this.pageCount = this.pageCount + 1;
    this.getRiskList();
  }

  previosPage() {
    this.pageCount = this.pageCount - 1;
    this.getRiskList()
  }
}
