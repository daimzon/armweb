import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-otp-check',
    templateUrl: './otp-check.component.html',
    styleUrls: ['./otp-check.component.scss']
})
export class OtpCheckComponent implements OnInit {

    disablePhone: boolean = false;
    disableOtp: boolean = false;
    phone: any;
    otp: any;

    constructor() { }

    ngOnInit() {
    }

    otpSent(type) {
        if (type === 'phone') this.disablePhone = true;
        else this.disableOtp = true;
    }
}
