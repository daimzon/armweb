import { Component, OnInit, Inject } from '@angular/core';
import {requestService} from '../../../services/request-list.service'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {NewRequestService} from '../../../services/new-request.service';
import { ToastrService } from 'ngx-toastr';

export interface DialogData {
  name: any;
  id: any;
}

@Component({
  selector: 'app-change-manager',
  templateUrl: './change-manager.component.html',
  styleUrls: ['./change-manager.component.scss']
})


export class ChangeManagerComponent  {

  users: any;
  selectedUser: any;
  comment: any;
  constructor(
    public service: NewRequestService,
    private toastr: ToastrService,
  @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) {
  }

  showError(text) {
    this.toastr.error(text);
  }

  showSuccess(text) {
    this.toastr.success(text);
  }

  accept() {
    let data = {
      'user' : this.selectedUser,
      'message' : this.comment
    }
    if (this.selectedUser === undefined) {
      let text = 'Выберите менеджера'
      this.showError(text)
    } else if (this.comment === undefined) {
      let text = 'Оставьте комментарий'
      this.showError(text)
    } else {
      console.log(data)
      this.service.changeUser(this.data.id, data).subscribe(
        (data: any) => {
          console.log(data)
          let text = 'Заявка переназначена'
          this.showSuccess(text)
        }, (err: any) => {
          let text = 'Произошла ошибка'
          this.showError(text)
        });
    }
  }

}
