import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientPhotoComponent } from './client-photo.component';

describe('ClientPhotoComponent', () => {
  let component: ClientPhotoComponent;
  let fixture: ComponentFixture<ClientPhotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientPhotoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientPhotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
