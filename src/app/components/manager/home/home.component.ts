import {Component, ViewEncapsulation, OnInit} from '@angular/core';
import {requestService} from '../../../services/request-list.service';
import {SP500} from './stock';
import {catalogService} from '../../../services/catalogs.service';
import {ChartType} from 'chart.js';


export interface Margin {
    top: number;
    right: number;
    bottom: number;
    left: number;
}

interface Stock {
    date: Date;
    price: number;
}

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
    donut = {
      'overal_sum': 0,
      'all_count': 0,
      'accepted_count': 0,
      'rejected_count': 0,
    };
    public doughnutChartLabels: string[] = ['План', 'Итог'];
    public doughnutChartData: number[] = [0, 0];
    public doughnutChartType: ChartType = 'doughnut';
    public doughnutChartOption: any = {
        legend: {
            display: true,
        },
        cutoutPercentage: 75,
    };
    public doughnutChartStyle: any = [{
        backgroundColor: ['rgb(245, 186, 65)']
    }];

    public barChartLabels: string[] = [];
    public barChartData: number[] = [];
    public barChartType = 'bar';
    public barChartOptions: any = {
        responsive: true,
        maintainAspectRatio: false,
        'backgroundColor': [
            '#FF6384',
            '#4BC0C0',
            '#FFCE56',
            '#E7E9ED',
            '#36A2EB'
        ],
    };

    requests: any;

    progressColor = 'warn';
    progressMode = 'determinate';
    progressValue = 50;
    progressDiameter: 50;
    myTeam: any;
    constructor(
        public reqService: requestService,
        public catalog: catalogService,
    ) {
    }

    ngOnInit() {
        this.getList();
        this.getStats();
        this.getTeam();
        this.getSelectedStats();
    }

    getTeam() {
        this.reqService.getBranchStatistics().subscribe(
            (data: any)  => {
                this.myTeam = data;
            }
        );
    }

  getSelectedStats() {
    this.reqService.selectedStats().subscribe(
      (data: any)  => {
        console.log(data);
        this.donut = {
          'overal_sum': data.overal_sum,
          'all_count': data.all_count,
          'accepted_count': data.accepted_count,
          'rejected_count': data.rejected_count,
        };

        this.doughnutChartData = [1000000, this.donut.overal_sum];
      }
    );
  }

    getList() {
        this.reqService.getRequestList().subscribe(
            data => {
                this.requests = data;
            });
    }

    getStats() {
        const _this = this;
        this.catalog.getLastMonthes().subscribe(
            (data: any) => {
                const array = data.results;
                array.forEach(function (obj) {
                    _this.barChartLabels.push(new Date(obj.date).toISOString().slice(0, 10));
                    _this.barChartData.push(obj.result);
                });
            }
        );
    }

}
