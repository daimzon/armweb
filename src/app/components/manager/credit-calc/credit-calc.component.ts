import {Component, OnInit} from '@angular/core';
import {LocalStorage} from '@ngx-pwa/local-storage';


@Component({
    selector: 'app-credit-calc',
    templateUrl: './credit-calc.component.html',
    styleUrls: ['./credit-calc.component.scss']
})
export class CreditCalcComponent implements OnInit {

    amount: any;
    term: any;
    result: any;
    rate: any;
    allAmount: any;
    percent: any;
    orgKom: any;
    showPaymentSchedule: boolean = false;
    totalComissionOrg: any;

    payType: string;

    totalPayment: number = 0;
    totalOverPayment: number = 0;

    pays: object;

    constructor(
        protected localStorage: LocalStorage
    ) {
    }

    ngOnInit() {
        this.getValues()
    }

    getValues() {
        this.localStorage.getItem('credit_amount').subscribe((data) => {
            this.amount = data; // null
            console.log()
        });

        this.localStorage.getItem('credit_term').subscribe((data) => {
            this.term = data; // null
        });
    }

    calc(type) {
        let newRate;
        let amountOneP;
        let comission;
        let totalAmount
        amountOneP = this.amount / 100
        newRate = this.rate / 100;
        comission = amountOneP * this.orgKom;
        this.totalComissionOrg = comission;
        totalAmount = this.amount - comission;
        let a = Math.pow(1 + newRate, this.term);
        this.result = totalAmount * newRate * a / (a - 1);

        this.allAmount = this.result * this.term;

        this.percent = this.allAmount - this.amount;

        this.payType = type;
    }

    calcSchedule(type) {
        function gaussRound(num, decimalPlaces) { // Правильное банковское округление, нужно вывести в глобал.
            let d = decimalPlaces || 0,
                m = Math.pow(10, d),
                n = +(d ? num * m : num).toFixed(8),
                i = Math.floor(n), f = n - i,
                e = 1e-8,
                r = (f > 0.5 - e && f < 0.5 + e) ?
                    ((i % 2 == 0) ? i : i + 1) : Math.round(n);
            return d ? r / m : r;
        }

        this.totalPayment = 0;
        this.totalOverPayment = 0;

        const monthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        let sum = this.amount,
            schedulePays = [],

            currentMonth = monthDays[0],
            calcMonths = monthDays;

        const currentMonthNumber = (new Date()).getMonth(),
            rate = Number(this.rate),
            term = this.term,
            monthPay = sum / term,

            interestRate = rate / 100 / 12;

        for (let i = 0; i < Math.ceil(term / 12); i++) {
            calcMonths = calcMonths.concat(monthDays);
        }

        function calculateAnnuity() {
            const pay = sum * (interestRate + (interestRate / (Math.pow((1 + interestRate), term) - 1)));

            function getRatePay() { // начисленные проценты
                return sum * interestRate;
            }

            function getPay() { // Платеж по кредиту
                return pay - getRatePay();
            }

            for (let i = 0; i < term; i++) {
                schedulePays.push(
                    {
                        'debtBalance': gaussRound(sum, 2), // остаток
                        'debt': gaussRound(getPay(), 2), //  платеж по кредиту
                        'interestPayment': gaussRound(getRatePay(), 2), // проценты
                        'totalPayment': gaussRound(pay, 2) // общая плата
                    }
                );
                sum = sum - getPay();
            }
        }

        function calculateDiff() {
            function getPercentPay() {
                return (sum * rate / 100) / 365 * currentMonth;
            }

            let monthCount = 1;
            for (let i = currentMonthNumber; i < term + 1; i++) {
                currentMonth = calcMonths[i];

                schedulePays.push(
                    {
                        'month': monthCount,
                        'days': currentMonth,
                        'debtBalance': gaussRound(sum, 2),
                        'debt': gaussRound(monthPay, 2),
                        'interestPayment': gaussRound(getPercentPay(), 2),
                        'totalPayment': gaussRound(getPercentPay() + monthPay, 2)
                    }
                );
                monthCount++;
                sum = sum - monthPay;
            }
        }

        if (type === 'annuity') calculateAnnuity();
        else if (type === 'diff') calculateDiff();

        let test = 0;

        for (let i = 0; i < schedulePays.length; i++) {
            this.totalPayment += schedulePays[i].totalPayment;
            this.totalOverPayment += schedulePays[i].interestPayment;
            test += schedulePays[i].debt;
        }

        this.totalPayment = gaussRound(this.totalPayment, 2);
        this.totalOverPayment = gaussRound(this.totalOverPayment, 2);

        this.pays = schedulePays;

        this.showPaymentSchedule = true;
    }
}
