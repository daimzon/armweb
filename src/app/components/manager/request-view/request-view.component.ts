import {Component, OnInit, Inject, TemplateRef} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {requestService} from '../../../services/request-list.service';
import {NewPhotoComponent} from '../new-photo/new-photo.component';
import {ChangeManagerComponent} from '../change-manager/change-manager.component';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
import {catalogService} from '../../../services/catalogs.service';
import {RiskService} from '../../../services/risk.service';
import { environment } from '../../../../environments/environment';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DomSanitizer} from '@angular/platform-browser';
import { HttpClient, HttpHeaders,  HttpEventType } from '@angular/common/http';
import {NewRequestService} from '../../../services/new-request.service';
import { ToastrService } from 'ngx-toastr';
import { clientService } from '../../../services/client.service';
import {saveAs as importedSaveAs} from 'file-saver';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

export interface DialogData {
  url: string;
  blobType: string;
  pkb: boolean;
}
@Component({
    selector: 'app-request-view',
    templateUrl: './request-view.component.html',
    styleUrls: ['./request-view.component.scss']
})
export class RequestViewComponent implements OnInit {
  clicked = false;
    filedata: any;
    request: any;
    fio: string;
    phone: string;
    residentialAddress: string;
    registrationAddress: string;
    credit: string;
    date: string;
    total_num: number;
    obj: Object;
    reject = false;
    test = false;
    purpose: string;
    ready = false;
    email: string;
    docDescrip: string;
    hideInfo = true;
    showButtonCaption = 'Подробнее';
    userRole: any;
    riskOfficer: any;
    users: any;
    off:FormGroup;
    offers: any;
    clickedd = false;
    done = false;
    dispalyOffers = false;
    history: any;
    messageField: any;
    displayDocs = false;
    signersList: any;
    selectSigner: any;
    signedDocs: any;
    loanReasonMessage: any;
    riskReasonMessage: any;
    redemptionList: any;
    offersRisks: any;
    randomDoc: any;
    randomDescription: any;
    showEditOffers = false;
    showEditSlary = true;

    apiEndpoint = environment.APIEndpoint;
    fileData: File = null;
    docs = {'files' : []};
    firstDocsList: any;
    firstDocs = false;
    riskSalary: any;
    buttonStatus = false;
    selectedCard: any;
    offerApplication: any;
    offerRepayment: any;
    offerAmount: any;
    offerTerm: any;
    offerRate: any;
    delButton = false;
    editButton = true;
    offerPaymentAmount: any;
    offerComissionRate: any;
    offerComissionSum: any;
    riskOffers = [];
    modalRef: BsModalRef;
    rejectArr: any;
    laTwo = false;
//////////////////////////////
    result: any;
    testTwo:any;
    allAmount: any;
    percent: any;
    showPaymentSchedule: boolean = false;
    totalComissionOrg: any;
    selectAccount: any;
    payType: number;

    totalPayment: number = 0;
    totalOverPayment: number = 0;
    checkLoan = false;
    accountList: any;
    pays: any;


    constructor(
      private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private getInfo: requestService,
        public dialog: MatDialog,
        private spinnerService: Ng4LoadingSpinnerService,
        public catalogServ: catalogService,
        public riskServ: RiskService,
        private http: HttpClient,
        public newReq: NewRequestService,
        public router: Router,
        private modalService: BsModalService,
        private toastr: ToastrService

    ) {
    }


    changeManager(): void {
      const reqId = this.route.snapshot.params['id'];
      const dialogRef = this.dialog.open(ChangeManagerComponent, {
            panelClass: 'calcDialog',
            width: '500px',
            data: {name: this.users, id: reqId}
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }
    openDialog(data, type, isPkb): void {
      const dialogRef = this.dialog.open(gcvpView, {
        data: {url: data, blobType: type, pkb: isPkb}
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      });
    }
    delDocs(data) {
      console.log(localStorage.getItem('permissions'));
      if (localStorage.getItem('permissions') == 'SM01' || localStorage.getItem('permissions') == 'SM02') {
        console.log(data.id);
        console.log(this.request.documents);
        this.request.documents = this.request.documents.filter(h => h !== data);
        console.log(this.request.documents);
        const desc = data.description;
        this.http.delete(this.apiEndpoint + '/api/v2/sales/application_universal_bind/' + data.id, )
        .subscribe(
          (data: any) => {
            console.log(data);
            const text = 'Документ ' + desc + ' успешно удален';
          this.showSuccess(text);
          });
      } else {
        const text = 'У вас недостаточно прав для выполнения этого действия';
        this.showError(text);
      }
      }

    editDocs(data) {
      const id = this.route.snapshot.params['id'];

      console.log(data);
      const desc = data.description;
      const formData = new FormData();
      formData.append('description', data.description);
      this.getInfo.changeName(data.id, formData)
      .subscribe(
        (data: any) => {
          console.log(data);
          const text = 'Название документа успешно изменено на ' + data.description;
        this.showSuccess(text);
        });
        confirm();
    }
    delDocsSigned(data) {
      console.log(data.id);
      console.log(this.request.documents_signed);
      this.request.documents_signed = this.request.documents_signed.filter(h => h !== data);
      console.log(this.request.documents_signed);
      const desc = data.description;
      this.http.delete(this.apiEndpoint + '/api/v2/sales/application_universal_bind/' + data.id, )
      .subscribe(
        (data: any) => {
          console.log(data);
          const text = 'Документ ' + desc + ' успешно удален';
        this.showSuccess(text);
        });
    }
    getClienAccounts() {
      const id = this.route.snapshot.params['id'];
      this.newReq.getClientAccounts(id).subscribe(
        (data: any) => {
          console.log(data);
          this.accountList = data
          console.log(this.accountList)
        }
      );
    }
    getDocs(data) {
      console.log(this.request.documents_signed);
      console.log(data);
      const fileType = data.split('.').pop();
      let blobType;
      let isPkb = false;
      if (fileType === 'html') {
        isPkb = true;
        blobType = 'text/html';
        this.openDialog(data, blobType, isPkb);
      } else if (fileType === 'pdf') {
        blobType = 'application/pdf';
        this.openDialog(data, blobType, isPkb);
      } else if (fileType === 'xlsx') {
        this.getInfo.getXls(data).subscribe(
          (response: any) => {
            console.log(data);
            importedSaveAs(response, 'report.xlsx');
          });
      } else if (fileType === 'jpg') {
        blobType = 'image/jpeg';
        this.openDialog(data, blobType, isPkb);
      } else if (fileType === 'jpeg') {
        blobType = 'image/jpeg';
        this.openDialog(data, blobType, isPkb);
      } else if (fileType === 'png') {
        blobType = 'image/png';
        this.openDialog(data, blobType, isPkb);
      }
    }
    openModal17(template17: TemplateRef<any>) {
      this.modalRef = this.modalService.show(template17);
    }
    confirm(): void {
      this.modalRef.hide();
    }
    openModal(template: TemplateRef<any>) {
      this.modalRef = this.modalService.show(template);
    }

    ngOnInit() {
        this.getUserRole();
        this.getTeamList();
        this.getSigners();
        if (localStorage.getItem('permissions') == 'SM01' || localStorage.getItem('permissions') == 'SM02') {
          this.delButton = true;
        }
        if (localStorage.getItem('permissions') == 'LA01' || localStorage.getItem('permissions') == 'LA02') {
          this.editButton = false;
        }
        if (localStorage.getItem('permissions') == 'LA02') {
          this.laTwo = true
        }
        this.off = this._formBuilder.group({
          signer: ['', Validators.required],
          //account: ['', Validators.required],
      });
    }

    getTeamList() {
        this.getInfo.geTeamList().subscribe(
            (data: any) => {
                this.users = data;
                console.log(data);
            }
        );
    }

    sendMessage() {
      const id = this.route.snapshot.params['id'];
      const data = {
        message: this.messageField
      };
      this.getInfo.getMessages(id, data).subscribe(
        (data: any) => {
          console.log(data);
        }
      );
    }

    getRequest() {
        const id = this.route.snapshot.params['id'];
        this.getInfo.getRequest(id).subscribe(
            (data: any) => {
                console.log(data);
                this.request = data;
                if(this.request.rejection_reason !== null){
                  this.rejectArr = this.request.rejection_reason.join(', ');
                  console.log(this.rejectArr);
                }
                console.log(this.rejectArr);
                if(this.request.status.id !== 6){
                  this.buttonStatus = true;
                }
                console.log(this.buttonStatus);
                this.total_num = parseInt(this.request.meta_information.additional_income, 10) + parseInt(this.request.additional_employment.salary, 10) + parseInt(this.request.employment_information.salary, 10);
                console.log(this.total_num);
                this.done = true;
                this.spinnerService.hide();
                console.log(this.request.status.id);
                if (data.status.id === 3) {
                    this.getFirstDocs();
                } else if (data.status.id === 6) {
                  this.getOffers();
                  // this.getClienAccounts();
                } else if (data.status.id === 8) {
                  this.displayDocs = true;
                  this.getSignedDocs();
                }

            }
        );
    }

  getSignedDocs() {
    const id = this.route.snapshot.params['id'];
    this.getInfo.getSignedDocs(id).subscribe(
      (data: any) => {
        console.log(data);
        this.signedDocs = data;
        this.displayDocs = true;
      }
    );
  }

  getRiskRequest() {
    this.getRedemption();
    const id = this.route.snapshot.params['id'];
    this.offerApplication = id;
    this.riskServ.readApplication(id).subscribe(
      (data: any) => {
        console.log(data);
        this.request = data;
        if(this.request.rejection_reason !== null){
          this.rejectArr = this.request.rejection_reason.join(', ');
          console.log(this.rejectArr);
        }

        this.done = true;
        if (data.status.id === 11) {
          this.showEditOffers = true;
          this.showEditSlary = false;
          this.test = true;
        }
        if(data.status.id !== 200){
          console.log('не выдано');
          this.ready = true;
        }
      }
    );
  }

  acceptRisk() {
    const id = this.route.snapshot.params['id'];
    const data = {
      'salary' : this.riskSalary
    };
    this.riskServ.acceptApplication(id, data).subscribe(
      (data: any) => {
        console.log(data);
        this.router.navigate(['manager/request-list']);
      }
    );
  }

  rejectRisk() {
    const data = {
      message: this.riskReasonMessage
    };
    const id = this.route.snapshot.params['id'];
    this.riskServ.rejectApplication(id, data).subscribe(
      (data: any) => {
        console.log(data);
        this.router.navigate(['manager/request-list']);
      }
    );
  }

    getLoanRequest() {
        const id = this.route.snapshot.params['id'];
        this.getInfo.getLoanRequest(id).subscribe(
            (data: any) => {
                console.log(data);
                this.request = data;
                if(this.request.rejection_reason !== null){
                  this.rejectArr = this.request.rejection_reason.join(', ');
                  console.log(this.rejectArr);
                }
                console.log(this.rejectArr);
                if(this.request.status.id !== 6){
                  this.buttonStatus = true;
                }
                if(data.status.id !== 200){
                  console.log('не выдано');
                  this.ready = true;
                } else{
                  console.log('выдан');
                }
                this.done = true;
                this.getLoanHistory();
            }
        );
    }

    acceptLoan() {
        const id = this.route.snapshot.params['id'];
        this.getInfo.acceptLoan(id).subscribe(
            (data: any) => {
                console.log(data);
                this.router.navigate(['manager/request-list']);
            }
        );
    }

    rejectLoan() {
        const id = this.route.snapshot.params['id'];
        this.getInfo.rejectLoan(id, this.loanReasonMessage).subscribe(
            (data: any) => {
                console.log(data);
                this.router.navigate(['manager/request-list']);
            }
        );
    }

    accountSelected(event) {
      this.selectedCard = event.value.id;
      console.log(this.selectedCard)

    }

    getOffers() {
      const id = this.route.snapshot.params['id'];
      this.getInfo.getOffers(id).subscribe(
        (data: any) => {
          console.log(data);
          this.offers = data;
          if (data.length > 0) {
            this.dispalyOffers = true;
          }
        }
      );
    }

    getLoanOffers() {
      const id = this.route.snapshot.params['id'];
      this.getInfo.getLoanOffers(id).subscribe(
        (data: any) => {
          console.log(data);
          this.offers = data;
          if (data.length > 0) {
            this.dispalyOffers = true;
          }
        }
      );
    }

  clietnReject() {
    const id = this.route.snapshot.params['id'];
    this.getInfo.clientReject(id).subscribe(
      (data: any) => {
        console.log(data);
        const text = 'Статус отказано клиентом';
        this.showSuccess(text);
        this.reject = true;
      },
      err =>{
        const text = 'Что-то пошло не так';
        this.showError(text);
      }
    );
  }


  selectOffer(id) {
    const data = {
      'offer' : id,
      'signer' : this.selectSigner,
      'card' : this.selectedCard
    };
    const requestId = this.route.snapshot.params['id'];
    this.getInfo.selectOffer(requestId, data).subscribe(
      (data: any) => {
        console.log(data);
        this.dispalyOffers = false;
        this.router.navigate(['manager/request-list']);
        this.modalRef.hide();
      },
      (err: any) => {
        this.showError(err.error.errorMessage);
        console.log(err);
      }
    );
  }

  getHistory() {
    const id = this.route.snapshot.params['id'];
    const data = {};
    this.getInfo.getHistory(id).subscribe(
      (data: any) => {
        console.log(data);
        data.forEach(element => {
          if(element.status.id === 6){
            this.getOffers();
          }
        });
        this.history = data.reverse();
      }
    );
  }

  getLoanHistory() {
    const id = this.route.snapshot.params['id'];
    const data = {};
    this.getInfo.getLoanHistory(id).subscribe(
      (data: any) => {
        console.log(data);
        data.forEach(element => {
          if(element.status.id === 6){
            this.checkLoan = true;
            this.getLoanOffers();

          } else{
            this.checkLoan = false;
          }
        });
        this.history = data.reverse();
      }
    );
  }

  getSigners() {
    const type = 'signers/';
    const id = this.route.snapshot.params['id'];
    this.catalogServ.getSigners(id, type).subscribe(
      (data: any) => {
        console.log(data);
        if (data.length > 0) {
          this.signersList = data;
        }
      }
    );
  }
  getUserRole() {
    this.spinnerService.show();
    const role = localStorage.getItem('permissions');

    this.userRole = role.includes('LA');
    this.riskOfficer = role.includes('RO');
    console.log(this.ready);

    if (this.userRole === true) {
        this.getLoanRequest();
    } else if (this.riskOfficer === true) {
        this.getRiskRequest();
    } else {
        this.getRequest();
        this.getHistory();
    }

    console.log('CHECK:' , this.userRole);
  }
    showHideInfo() {
        this.hideInfo = !this.hideInfo;
        this.showButtonCaption = 'Свернуть';
    }

  sentDocs() {
    const id = this.route.snapshot.params['id'];

    if (this.signedDocs.length > this.docs.files.length) {
      const text = 'Загрузите документы';
      this.showError(text);
    } else {
      this.newReq.sentSignedDocs(id, this.docs).subscribe(
        (data: any) => {
          console.log(data);
          this.router.navigate(['manager/request-list']);
        }
      );
    }
  }

  fileEvent(e) {
    this.filedata = e.target.files[0];
    console.log(e);

  }

  fileEventNew(e, name, typeId) {
    this.filedata = e.target.files[0];
    console.log(e);
    this.spinnerService.show()
    const formData = new FormData();
    formData.append('document', this.filedata);
    formData.append('description', name);
    let text

    this.http.post(this.apiEndpoint + '/api/v2/sales/application_documents/', formData, )
      .subscribe(
        (data: any) => {
          this.docs.files.push({'document_id': data.id, 'document_type': typeId});
          (<HTMLInputElement>document.getElementById(typeId)).value = 'Файл загружен';
          console.log(this.docs);
          this.spinnerService.hide();
          text = 'Документ загружен';
          this.showSuccess(text);

        }),
      (err : any) => {
        text = 'Техническая ошибка'
        this. showError(text)
      };
  }

  uploadData(name, typeId) {
    this.spinnerService.show()
    const formData = new FormData();
    formData.append('document', this.filedata);
    formData.append('description', name);
    let text

    this.http.post(this.apiEndpoint + '/api/v2/sales/application_documents/', formData, )
      .subscribe(
        (data: any) => {
          this.docs.files.push({'document_id': data.id, 'document_type': typeId});
          (<HTMLInputElement>document.getElementById(typeId)).value = 'Файл загружен';
          console.log(this.docs);
          this.spinnerService.hide();
          text = 'Документ загружен';
          this.showSuccess(text);

        }),
      (err : any) => {
        text = 'Техническая ошибка'
        this. showError(text)
    };
  }

  uploadRandom() {
    const id = this.route.snapshot.params['id'];
    this.spinnerService.show()
    const formData = new FormData();
    formData.append('document', this.filedata);
    formData.append('description', this.randomDescription);
    formData.append('application', id);

    this.http.post(this.apiEndpoint + '/api/v2/sales/application_universal_bind/', formData, )
      .subscribe(
        (data: any) => {
          console.log(data);
          const text = 'Документ загружен';
          this.showSuccess(text);
          this.spinnerService.hide();
          this.getRequest();
          this.randomDescription = '';
          (<HTMLInputElement>document.getElementById('random')).value = '';
        },
        (err: any) => {
          const text = 'Техническая ошибка';
          this.showError(text);
          this.spinnerService.hide()
        })
        ;
  }

  checkServiceStatus() {
      let text;
    this.catalogServ.getGcvpStatus().subscribe(
      (data: any) => {
        console.log(data);
         if (data.is_gcvp_working === 'true' && data.is_pkb_working === 'true') {
           this.sentFirstDocs();
         } else {
           text = 'Сервисы не доступны, попробуйте позже';
           this.showError(text);
         }
      }
    );
  }

  sentFirstDocs() {
      const id = this.route.snapshot.params['id'];
      if (this.firstDocsList.length > this.docs.files.length) {
        const text = 'Загрузите документы';
        this.showError(text);
      } else {
        this.newReq.sentFirstSignedDocs(id, this.docs).subscribe(
          (data: any) => {
            console.log(data);
            this.router.navigate(['manager/request-list']);
          }
        );
      }
  }

  getFirstDocs() {
        this.firstDocs = true;
      const id = this.route.snapshot.params['id'];
      this.http.get(this.apiEndpoint + '/api/v2/sales/applications/' + id + '/first_upload_documents/', )
          .subscribe(
              (data: any) => {
                  this.firstDocsList = data;
                  console.log(this.firstDocsList);
              });
  }

  showError(text) {
    this.toastr.error(text);
  }

  showSuccess(text) {
    this.toastr.success(text);
  }

  getRedemption() {
    const type = 'redemption_method';
    this.catalogServ.getDynamicCatalog(type).subscribe(
      (data: any) => {
        this.redemptionList = data.items;
        console.log(this.redemptionList);
      }
    );
  }
  typeRepayment(value){
    this.payType = value;
    console.log(this.payType);
    this.calcSchedule();

  }
  offersDone() {
    const id = this.route.snapshot.params['id'];
    this.newReq.riskOfferAdd(id).subscribe(
      (response: any) => {
        console.log(response);
        this.router.navigate(['manager/request-list']);
        const text = 'Офферы успешно отправлены';
        this.showSuccess(text);
      },
      (err: any) => {
        const text = 'Техническая ошибка';
        this.showError(text);
      }
    );

  }


calcSchedule() {
  if(this.request.client_category === 'C' || this.request.client_category === 'B1' || this.request.client_category === 'B2' ){
    if (this.offerTerm >= 6 && this.offerTerm <= 12) {
      this.offerRate = 10;
      this.offerComissionRate = 7;
    } else if (this.offerTerm >= 13 && this.offerTerm <= 24) {
      this.offerRate = 17;
      this.offerComissionRate = 8;
    } else if (this.offerTerm >= 25 && this.offerTerm <= 36) {
      this.offerRate = 19;
      this.offerComissionRate = 8;
    } else if (this.offerTerm >= 37 && this.offerTerm <= 60) {
      this.offerRate = 22;
      this.offerComissionRate = 8;
    }
  } else if(this.request.client_category === 'A1' || this.request.client_category === 'A2'){
    if (this.offerTerm >= 6 && this.offerTerm <= 12) {
      this.offerRate = 10;
      this.offerComissionRate = 6;
    } else if (this.offerTerm >= 13 && this.offerTerm <= 24) {
      this.offerRate = 16;
      this.offerComissionRate = 7;
    } else if (this.offerTerm >= 25 && this.offerTerm <= 36) {
      this.offerRate = 18;
      this.offerComissionRate = 7;
    } else if (this.offerTerm >= 37 && this.offerTerm <= 60) {
      this.offerRate = 19;
      this.offerComissionRate = 7;
    }
  } else if(this.request.client_category === 'A3'){
    if (this.offerTerm >= 6 && this.offerTerm <= 12) {
      this.offerRate = 8;
      this.offerComissionRate = 5;
    } else if (this.offerTerm >= 13 && this.offerTerm <= 24) {
      this.offerRate = 13;
      this.offerComissionRate = 5;
    } else if (this.offerTerm >= 25 && this.offerTerm <= 60) {
      this.offerRate = 15;
      this.offerComissionRate = 5;
    }
  } else if(this.request.client_category === 'A4'){
    if (this.offerTerm >= 6 && this.offerTerm <= 60) {
      this.offerRate = 16.5;
      this.offerComissionRate = 5;
    }
  }



    function gaussRound(num, decimalPlaces) { // Правильное банковское округление, нужно вывести в глобал.
        let d = decimalPlaces || 0,
            m = Math.pow(10, d),
            n = +(d ? num * m : num).toFixed(8),
            i = Math.floor(n), f = n - i,
            e = 1e-8,
            r = (f > 0.5 - e && f < 0.5 + e) ?
                ((i % 2 == 0) ? i : i + 1) : Math.round(n);
        return d ? r / m : r;
    }

    this.totalPayment = 0;
    this.totalOverPayment = 0;

    const monthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    let sum = parseInt(this.offerAmount),
        schedulePays = [],

        currentMonth = monthDays[0],
        calcMonths = monthDays;

    const currentMonthNumber = (new Date()).getMonth(),
    offerRate = Number(this.offerRate),
        offerTerm = parseInt(this.offerTerm),
        monthPay = sum / offerTerm,

        interestRate = offerRate / 100 / 12;
        console.log(currentMonthNumber, interestRate);
        this.testTwo = monthPay;

    for (let i = 0; i < Math.ceil(offerTerm / 12); i++) {
        calcMonths = calcMonths.concat(monthDays);
    }
    this.offerComissionSum = (sum * this.offerComissionRate)/100;
    console.log(this.offerComissionSum);

    function calculateAnnuity() {
      console.log(interestRate, offerTerm);
        const pay = sum * (interestRate + (interestRate / (Math.pow((1 + interestRate), offerTerm) - 1)));
        console.log(pay);



        function getRatePay() { // начисленные проценты
            return sum * interestRate;
        }

        function getPay() { // Платеж по кредиту
            return pay - getRatePay();
        }

        for (let i = 0; i < offerTerm; i++) {
            schedulePays.push(
                {
                    'interestPayment': gaussRound(getRatePay(), 2), // проценты
                    'totalPayment': gaussRound(pay, 2) // общая плата
                }
            );
            sum = sum - getPay();
        }
        console.log(schedulePays);
    }

    function calculateDiff() {
        function getPercentPay() {
            return (sum * offerRate / 100) / 365 * currentMonth;
        }

        let monthCount = 1;
        console.log(monthPay);
        const abc = gaussRound(getPercentPay() + monthPay, 2);
            console.log(abc, offerTerm);
        for (let i = currentMonthNumber; i < offerTerm + 1; i++) {
            schedulePays.push(
                {
                    'interestPayment': gaussRound(getPercentPay(), 2),
                    'totalPayment': gaussRound(getPercentPay() + monthPay, 2)
                }
            );
            monthCount++;
            sum = sum - monthPay;
        }
        console.log(schedulePays);
    }

    if (this.payType === 44) calculateAnnuity();
        else if (this.payType === 45) calculateDiff();

    let test = 0;

    for (let i = 0; i < schedulePays.length; i++) {
        this.totalPayment += schedulePays[i].totalPayment;
        this.totalOverPayment += schedulePays[i].interestPayment;
        test += schedulePays[i].debt;
    }

    this.totalPayment = gaussRound(this.totalPayment, 2);
    this.totalOverPayment = gaussRound(this.totalOverPayment, 2);

    this.pays = schedulePays;
    console.log(this.pays);
    this.pays.forEach(element => {
      console.log(element.totalPayment);
      this.offerPaymentAmount = element.totalPayment;
    });

    this.showPaymentSchedule = true;
    console.log(this.totalPayment);

}
  addOffer() {
    const data = {
      application: this.offerApplication,
      repayment_type: this.offerRepayment,
      amount: this.offerAmount,
      term: this.offerTerm,
      rate: this.offerRate,
      payment_amount: this.offerPaymentAmount,
      commission_rate: this.offerComissionRate,
      commission_sum: this.offerComissionSum,
    };
    this.riskOffers.push(data);
    console.log(this.riskOffers);
    let text;
    if(this.request.client_category === 'C'){
      if (this.offerRepayment === undefined) {
        text = 'Выберете тип погашения';
        this.showError(text);
      } else if (this.offerAmount === undefined) {
        text = 'Введите сумму';
        this.showError(text);
      } else if (this.offerAmount < 200000) {
        text = 'Сумма займа меньше возможной';
        this.showError(text);
      } else if (this.offerAmount >  1000000) {
        text = 'Сумма займа больше возможной';
        this.showError(text);
      } else if (this.offerTerm === undefined) {
        text = 'Введите срок';
        this.showError(text);
      } else if (this.offerTerm < 6) {
        text = 'Cрок займа меньше возможного';
        this.showError(text);
      } else if (this.offerTerm > 60) {
        text = 'Cрок займа больше возможного';
        this.showError(text);
      }  else if (this.offerComissionSum === undefined) {
        text = 'Введите сумму комиссии';
        this.showError(text);
      } else {
        this.spinnerService.show();
        this.newReq.createRiskOffer(data).subscribe(
          (response: any) => {
            console.log(response);
            this.getRiskRequest();
            const text = 'Оффер успешно создан';
            this.showSuccess(text);
          },
          (err: any) => {
            const text = 'Техническая ошибка';
            this.showError(text);
          }
        );
      }
    } else if(this.request.client_category === 'B1' || this.request.client_category === 'B2' || this.request.client_category === 'A1'){
      if (this.offerRepayment === undefined) {
        text = 'Выберете тип погашения';
        this.showError(text);
      } else if (this.offerAmount === undefined) {
        text = 'Введите сумму';
        this.showError(text);
      } else if (this.offerAmount < 200000) {
        text = 'Сумма займа меньше возможной';
        this.showError(text);
      } else if (this.offerAmount > 6000000) {
        text = 'Сумма займа больше возможной';
        this.showError(text);
      } else if (this.offerTerm === undefined) {
        text = 'Введите срок';
        this.showError(text);
      } else if (this.offerTerm < 6) {
        text = 'Cрок займа меньше возможного';
        this.showError(text);
      } else if (this.offerTerm > 60) {
        text = 'Cрок займа больше возможного';
        this.showError(text);
      }  else if (this.offerComissionSum === undefined) {
        text = 'Введите сумму комиссии';
        this.showError(text);
      } else {
        this.spinnerService.show();
        this.newReq.createRiskOffer(data).subscribe(
          (response: any) => {
            console.log(response);
            this.getRiskRequest();
            const text = 'Оффер успешно создан';
            this.showSuccess(text);
          },
          (err: any) => {
            const text = 'Техническая ошибка';
            this.showError(text);
          }
        );
      }
    } else if(this.request.client_category === 'A2' || this.request.client_category === 'A4'){
      if (this.offerRepayment === undefined) {
        text = 'Выберете тип погашения';
        this.showError(text);
      } else if (this.offerAmount === undefined) {
        text = 'Введите сумму';
        this.showError(text);
      } else if (this.offerAmount < 100000) {
        text = 'Сумма займа меньше возможной';
        this.showError(text);
      } else if (this.offerAmount > 6000000) {
        text = 'Сумма займа больше возможной';
        this.showError(text);
      } else if (this.offerTerm === undefined) {
        text = 'Введите срок';
        this.showError(text);
      } else if (this.offerTerm < 6) {
        text = 'Cрок займа меньше возможного';
        this.showError(text);
      } else if (this.offerTerm > 60) {
        text = 'Cрок займа больше возможного';
        this.showError(text);
      }  else if (this.offerComissionSum === undefined) {
        text = 'Введите сумму комиссии';
        this.showError(text);
      } else {
        this.spinnerService.show();
        this.newReq.createRiskOffer(data).subscribe(
          (response: any) => {
            console.log(response);
            this.getRiskRequest();
            const text = 'Оффер успешно создан';
            this.showSuccess(text);
          },
          (err: any) => {
            const text = 'Техническая ошибка';
            this.showError(text);
          }
        );
      }
    } else if(this.request.client_category === 'A3'){
      if (this.offerRepayment === undefined) {
        text = 'Выберете тип погашения';
        this.showError(text);
      } else if (this.offerAmount === undefined) {
        text = 'Введите сумму';
        this.showError(text);
      } else if (this.offerAmount < 100000) {
        text = 'Сумма займа меньше возможной';
        this.showError(text);
      } else if (this.offerAmount > 7000000) {
        text = 'Сумма займа больше возможной';
        this.showError(text);
      } else if (this.offerTerm === undefined) {
        text = 'Введите срок';
        this.showError(text);
      } else if (this.offerTerm < 6) {
        text = 'Cрок займа меньше возможного';
        this.showError(text);
      } else if (this.offerTerm > 60) {
        text = 'Cрок займа больше возможного';
        this.showError(text);
      }  else if (this.offerComissionSum === undefined) {
        text = 'Введите сумму комиссии';
        this.showError(text);
      } else {
        this.spinnerService.show();
        this.newReq.createRiskOffer(data).subscribe(
          (response: any) => {
            console.log(response);
            this.getRiskRequest();
            const text = 'Оффер успешно создан';
            this.showSuccess(text);
          },
          (err: any) => {
            const text = 'Техническая ошибка';
            this.showError(text);
          }
        );
      }
    }

  }

  deleteOffer(id) {
    this.newReq.riskOfferDelete(id).subscribe(
      (response: any) => {
        console.log(response);
        const text = 'Оффер успешно удален';
        this.getRiskRequest();
        this.showSuccess(text);
      },
      (err: any) => {
        const text = 'Техническая ошибка';
        this.showError(text);
      }
      );
  }

  // getRiskOffers() {
  //   const id = this.route.snapshot.params['id'];
  //   this.getInfo.getRiskOffer(id).subscribe(
  //     (data: any) => {
  //       console.log(data);
  //       this.offersRisks = data;
  //     }
  //   );
  // }


}

@Component({
  selector: 'gcvp-dialog',
  templateUrl: 'gcvp-dialog.html',
})
export class gcvpView {
  apiEndpoint = environment.APIEndpoint;
  gcvpUrl: any;
  url: any;
  constructor(
    public dialogRef: MatDialogRef<gcvpView>,
    private sanitizer: DomSanitizer,
    private http: HttpClient,
    public client: clientService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    console.log(this.data);
    this.url = this.http.get (this.apiEndpoint + this.data.url,  { headers: new HttpHeaders({
        'Content-Type': 'Content-Type:text/html; charset=utf-8',
      }), responseType: 'blob'}).subscribe(
      (response: any) => {
        const myBlob =  new Blob( [response] , {type: this.data.blobType});
        const url = URL.createObjectURL(myBlob);
        this.gcvpUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
      });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  print() {
    const iframe = document.getElementById('gcvpFrame') as HTMLIFrameElement;
    iframe.contentWindow.print();
  }

}
