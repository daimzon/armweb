import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {Form, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LocalStorage} from '@ngx-pwa/local-storage';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {FormControl} from '@angular/forms';
import { HttpClient, HttpEventType } from '@angular/common/http';
import {catalogService} from '../../../services/catalogs.service';
import {requestService} from '../../../services/request-list.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import {TranslateService} from '@ngx-translate/core';
import {CreditCalcComponent} from '../credit-calc/credit-calc.component';
import {NewPhotoComponent} from '../new-photo/new-photo.component';
import {Router, ActivatedRoute} from '@angular/router';
import { environment } from '../../../../environments/environment';
import {NewRequestService} from '../../../services/new-request.service';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
import { MatStepper } from '@angular/material';
import {DateAdapter, MAT_DATE_FORMATS} from '@angular/material/core';
import { AppDateAdapter, APP_DATE_FORMATS } from './format-datepicker';

export interface DialogData {
  typeId: string;
  name: string;
}
@Component({
    selector: 'app-new-profile',
    templateUrl: './new-profile.component.html',
    styleUrls: ['./new-profile.component.scss'],
    providers: [
      {provide: DateAdapter, useClass: AppDateAdapter},
      {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
    ]
})

export class NewProfileComponent implements OnInit {
    isLinear =  true;
    clicked = false;
    birthDate: string;

    firstFormGroup: FormGroup;
    secondFormGroup: FormGroup;
    thirdFormGroup: FormGroup;
    fourFormGroup: FormGroup;


    firstStep: FormGroup;
    secondStep: FormGroup;
    thirdStep: FormGroup;
    fourStep: FormGroup;
    fiveStep: FormGroup;
    sixStep: FormGroup;
    sevenStep: FormGroup;
    eightStep: FormGroup;
    nineStep: FormGroup;
    tenStep: FormGroup;
    elevenStep: FormGroup;
    smsStep: FormGroup;
    docStep: FormGroup;
    twelveStep: FormGroup;

    showAll = false;
    creditAmount: any;
    creditTerm: any;
    dateOfb: any;
    result: any;
    stateCtrl = new FormControl();
    checkboxFlag: any;
    liveAddress: any;
    docAddress: any;
    profileStep = 1;
    smsSent = false;
    creditGoal: any;
    commission = true;
    secondRelationshipOther = false;
    // Form data var's

    jobCategoty: any;
    education: any;
    marialStatus: any;
    productType: any;
    totalOverpayment: any = '0';
    // Catalog's list var's

    countryList: any;
    regionList: any;
    areaList: any;

    countryLiveList: any;
    regionLiveList: any;
    areaLiveList: any;

    jobCategoryList: any;
    educationList: any;
    marialStatusList: any;
    productTypeList: any;
    creditGoalList: any;
    filedata: any;

    countryReg: any;
    regionReg: any;
    areaReg: any;
    streetReg: any;
    homeReg: any;
    apartmentReg: any;

    countryLive: any;
    regionLive: any;
    areaLive: any;
    streetLive: any;
    homeLive: any;
    apartmentLive: any;
    public imagePath;
    imgURL: any;
    imgURLTwo: any;
    imgURLThree: any;
    channelsList: any;
    public message: string;
    fileData: File = null;
    docs = {'files' : []};
    docTypeId: any;
    genderList: any;
    docIssuers: any;
    diffHour: any;
    isMarial = false;
    isSpouseWork = false;
    isAdditionalWork = false;
    relationshipOther = false;
    docIssuersOther = false;
    channelOther = false;
    purposeOther = false;
    cityList: any;
    cityListLive: any;
    cityLiveListStatus = false;
    selected = 'false'

    loadButtonStatus = false;
    uploadForm: FormGroup = this._formBuilder.group({
      media: [null, Validators.required]
    });
    constructor(
        private _formBuilder: FormBuilder,
        protected localStorage: LocalStorage,
        private http: HttpClient,
        public catalogs: catalogService,
        public router: Router,
        public snackBar: MatSnackBar,
        public dialog: MatDialog,
        public request: requestService,
        private route: ActivatedRoute,
        public newReq: NewRequestService,
        private spinnerService: Ng4LoadingSpinnerService,
        private toastr: ToastrService

    ) {
    }
    @ViewChild('inputFile', {static: false}) myInputVariable: ElementRef;
    userFio: any;
    userName: any;
    userLastName: any;
    userFatherName: any;
    userIin: any;
    userDateOfb: any;
    userTaxName: any;
    clientAddres: any;
    totalCredit: any = '0';
    docReq: any;
    redemptionList: any;
    redemptionType: any;
    docList: any;
    ownershipList: any;
    relationDegree: any;
    inputDis = false;
    apiEndpoint = environment.APIEndpoint;
    productTypeLoaded: any;
    gcpvList: any;
    gcvpType = false;
    pkbType = false;
    cityListStatus = false;
    cardTypes: any;

    showError() {
        this.toastr.error('Проверьте поля');
    }
    showErrorr(text) {
      this.toastr.error(text);
  }
    showDocError() {
      this.toastr.error('Загрузите все документы');
    }

    showSuccess(text) {
        this.toastr.success(text);
    }


    openPhotoDialog(docTypeId, docName, docId): void {
        this.docs.files = this.docs.files.filter(function( obj ) {
          return obj.document_type !== 0;
        });
        console.log(this.docs);
        const dialogRef = this.dialog.open(NewPhotoComponent, {
            panelClass: 'calcDialog',
            width: '900px',
            height: '450px',
            data: {typeId: docTypeId, name: docName}
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log(result);
            if (this.docs.files.find(x => x.document_type === docId)) {
              this.docs.files = this.docs.files.filter(function( obj ) {
                return obj.document_type  !== docId;
              });
              this.docs.files.push({'document_id': result.data.id, 'document_type': docId});
            } else {
              this.docs.files.push({'document_id': result.data.id, 'document_type': docId});
            }
            (<HTMLInputElement>document.getElementById(docId)).value = 'Файл загружен';
        });
    }

    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
            duration: 2000,
        });
    }

    ngOnInit() {
        this.firstStep = this._formBuilder.group({
            creditAmount: ['', Validators.required],
            creditTerm: [this.userName, Validators.required],
            productType: [Validators.required],
            redemptionType: ['', Validators.required],
            commission: ['', Validators.required],
            maturityDate: ['', Validators.required]
        });

        this.secondStep = this._formBuilder.group({
            name: [''],
            surname: ['', Validators.required],
            patronymic: [''],
            userDateOfb: ['', Validators.required],
            male: ['', Validators.required],
            clientCitizen: ['', Validators.required],
        });

        this.thirdStep = this._formBuilder.group({
            additional_number: [''],
            home_number: [''],
            email: [''],
        });

        this.fourStep = this._formBuilder.group({
            first_name: ['', Validators.required],
            last_name: ['', Validators.required],
            patronymic_name: [''],
            relation_type: ['', Validators.required],
            mobile_number: ['', Validators.required],
            relation_type_other: [''],
        });

        this.twelveStep = this._formBuilder.group({
          first_name: ['', Validators.required],
          last_name: ['', Validators.required],
          patronymic_name: [''],
          relation_type: ['', Validators.required],
          mobile_number: ['', Validators.required],
          relation_type_other: [''],
        });

        this.fiveStep = this._formBuilder.group({
            organization_name: ['', Validators.required],
            organization_address: ['', Validators.required],
            total_work_exp: ['', Validators.required],
            current_work_exp: ['', Validators.required],
            salary: ['', Validators.required],
            work_mobile_number: ['', Validators.required],
            position: ['', Validators.required],
            job_category: ['', Validators.required],
            additionalNumber: [''],
            additionalWork: [''],
        });

        this.sixStep = this._formBuilder.group({
            organization_name: ['', Validators.required],
            organization_address: ['', Validators.required],
            total_work_exp: ['', Validators.required],
            current_work_exp: ['', Validators.required],
            salary: ['', Validators.required],
            work_mobile_number: ['', Validators.required],
            position: ['', Validators.required],
            job_category: ['', Validators.required],
            additionalNumber: [''],
            additionalWork: [''],
        });

        this.sevenStep = this._formBuilder.group({
            number: ['', Validators.required],
            issue_date: ['', Validators.required],
            expire_date: ['', Validators.required],
            issuer: ['', Validators.required],
            place_of_birth: ['', Validators.required],
            id_card_type: ['', Validators.required],

        });

        this.eightStep = this._formBuilder.group({
            loan_purpose: ['', Validators.required],
            channel: ['', Validators.required],
            loan_purpose_other: [''],
            channel_other: [''],
            children_15: ['', Validators.required],
            children_15_21: ['', Validators.required],
            additional_income: ['', Validators.required],
            expenses: ['', Validators.required],
            is_car_owner: ['', Validators.required],
            education: ['', Validators.required],
            family_status: ['', Validators.required],
            gcvp_type: ['', Validators.required],
            gcvp_category: [''],
            pkb_debt: ['', Validators.required],
        });

        this.nineStep = this._formBuilder.group({
            first_name: ['', Validators.required],
            last_name: ['', Validators.required],
            patronymic_name: [''],
            is_employed: ['', Validators.required],
            work_mobile_number: [''],
            organization_name: [''],
            organization_address: [''],
            position: [''],
            salary: [''],
        });

        this.tenStep = this._formBuilder.group({
            countryReg: ['', Validators.required],
            regionReg: ['', Validators.required],
            areaReg: ['', Validators.required],
            streetReg: ['', Validators.required],
            homeReg: ['', Validators.required],
            cityReg: [''],
            apartmentReg: [''],
            index: [''],
            check: [''],
            affiliationReg: ['', Validators.required],
        });

        this.elevenStep = this._formBuilder.group({
            countryLive: ['', Validators.required],
            regionLive: ['', Validators.required],
            areaLive: ['', Validators.required],
            streetLive: ['', Validators.required],
            homeLive: ['', Validators.required],
            cityLive: [''],
            apartmentLive: [''],
            index: [''],
            affiliationLive: ['', Validators.required],
        });

        this.smsStep = this._formBuilder.group({

        });

        this.docStep = this._formBuilder.group({

        });

        // Get catalogs data
        this.getRequestInfo();
        this.getJobCategory();
        this.getEducationList();
        this.getMarialStatusList();
        this.getProductType();
        this.getCountryList();
        this.getLiveCountryList();
        this.getCreditGoalList();
        this.getDocRequired();
        this.getRedemption();
        this.getChannelsList();
        this.getGender();
        this.getIssuers();
        this.getOwnership();
        this.getRelationDegree();
        this.getGcvpList();
        this.getDocTypes();
        
        //console.log(this.sevenStep.controls['expire_date'].value);
        
      
    }



  getInfo() {
    const id = this.route.snapshot.params['id'];
    this.request.getNewRequestInfo(id).subscribe((data: any) => {
      console.log(data);
      this.creditAmount = data.bid_amount;
    });
  }


    personUpdate(stepper: MatStepper) {
        let text;
        this.spinnerService.show();
        const id = this.route.snapshot.params['id'];
        const a = this.secondStep.controls['userDateOfb'].value.substring(0,2);
        const b = this.secondStep.controls['userDateOfb'].value.substring(2,4);
        const c = this.secondStep.controls['userDateOfb'].value.substring(4,8);
        const val = c + '-' + b + '-' + a;
        console.log(val);
        
        const data = {
            'first_name' : this.secondStep.controls['name'].value,
            'last_name' : this.secondStep.controls['surname'].value,
            'patronymic_name' : this.secondStep.controls['patronymic'].value,
            'birth_date' : val,
            'citizenship' : this.secondStep.controls['clientCitizen'].value,
            'gender' : this.secondStep.controls['male'].value,
        };
        this.newReq.personUpdate(id, data).subscribe(
            (data: any) => {
                console.log(data);
                stepper.next();
                text = 'Клиентские данные сохранены';
                this.showSuccess(text);
                this.spinnerService.hide();
            },
            err => {
                console.log(err);
                this.showError();
                this.spinnerService.hide();
            }
        );
    }

    contactUpdate(stepper: MatStepper) {
        let text;
        this.spinnerService.show();
        const id = this.route.snapshot.params['id'];
        const data = {
            'additional_number' : this.thirdStep.controls['additional_number'].value,
            'home_number' : this.thirdStep.controls['home_number'].value,
            'email' : this.thirdStep.controls['email'].value,
        };
        this.newReq.contactUpdate(id, data).subscribe(
            (data: any) => {
                console.log(data);
                stepper.next();
                text = 'Данные документа сохранены';
                this.showSuccess(text);
                this.spinnerService.hide();
            },
            err => {
                console.log(err);
                this.showError();
                this.spinnerService.hide();
            }
        );
    }

    contactPersonUpdate(stepper: MatStepper) {
        let text;
        this.spinnerService.show();
        const id = this.route.snapshot.params['id'];
        let relType;
        let relTypeNum;
        if (this.fourStep.controls['relation_type'].value.id === 9) {
          relType = this.fourStep.controls['relation_type_other'].value;
          relTypeNum = 9;
        } else {
          relType  = this.fourStep.controls['relation_type'].value.description;
          relTypeNum = this.fourStep.controls['relation_type'].value.id;
        }
        const data = {
            'first_name' : this.fourStep.controls['first_name'].value,
            'last_name' : this.fourStep.controls['last_name'].value,
            'patronymic_name' : this.fourStep.controls['patronymic_name'].value,
            'relation_type' : relType,
            'relation_type_id' : relTypeNum,
            'mobile_number' : this.fourStep.controls['mobile_number'].value,
        };

        this.newReq.contactPersonUpdate(id, data).subscribe(
            (data: any) => {
                console.log(data);
                stepper.next();
                text = 'Контактные данные сохранены';
                this.showSuccess(text);
                this.spinnerService.hide();
            },
            err => {
                console.log(err);
                this.showError();
                this.spinnerService.hide();
            }
        );
    }

  contactSecondPersonUpdate(stepper: MatStepper) {
    let text;
    this.spinnerService.show();
    const id = this.route.snapshot.params['id'];
    let relType;
    let relTypeNum;
    if (this.twelveStep.controls['relation_type'].value.id === 9) {
      relType = this.twelveStep.controls['relation_type_other'].value;
      relTypeNum = 9;
    } else {
      relType  = this.twelveStep.controls['relation_type'].value.description;
      relTypeNum = this.twelveStep.controls['relation_type'].value.id;
    }
    const data = {
      'first_name' : this.twelveStep.controls['first_name'].value,
      'last_name' : this.twelveStep.controls['last_name'].value,
      'patronymic_name' : this.twelveStep.controls['patronymic_name'].value,
      'relation_type' : relType,
      'relation_type_id' : relTypeNum,
      'mobile_number' : this.twelveStep.controls['mobile_number'].value,
    };
    this.newReq.contactPersonUpdate(id, data).subscribe(
      (data: any) => {
        console.log(data);
        stepper.next();
        text = 'Контактное лицо сохранено';
        this.showSuccess(text);
        this.spinnerService.hide();
      },
      err => {
        console.log(err);
        this.showError();
        this.spinnerService.hide();
      }
    );
  }

    employmentInformationUpdate(stepper: MatStepper) {
        let text;
        this.spinnerService.show();
        const id = this.route.snapshot.params['id'];
        const data = {
            'organization_name' : this.fiveStep.controls['organization_name'].value,
            'organization_address' : this.fiveStep.controls['organization_address'].value,
            'total_work_exp' : this.fiveStep.controls['total_work_exp'].value,
            'current_work_exp' : this.fiveStep.controls['current_work_exp'].value,
            'salary' : parseFloat(this.fiveStep.controls['salary'].value),
            'work_mobile_number' : this.fiveStep.controls['work_mobile_number'].value,
            'position' : this.fiveStep.controls['position'].value,
            'job_category' : this.fiveStep.controls['job_category'].value,
            'internal_phone' : this.fiveStep.controls['additionalNumber'].value,
        };
        this.newReq.employmentInformationUpdate(id, data).subscribe(
            (data: any) => {
                console.log(data);
                stepper.next();
                text = 'Данные о работе сохранены';
                this.showSuccess(text);
                this.spinnerService.hide();
            },
            err => {
                console.log(err);
                this.showError();
                this.spinnerService.hide();
            }
        );
    }

    additionalEmploymentInformationUpdate(stepper: MatStepper) {
        let text;
        this.spinnerService.show();
        const id = this.route.snapshot.params['id'];
        const data = {
            'organization_name' : this.sixStep.controls['organization_name'].value,
            'organization_address' : this.sixStep.controls['organization_address'].value,
            'total_work_exp' : this.sixStep.controls['total_work_exp'].value,
            'current_work_exp' : this.sixStep.controls['current_work_exp'].value,
            'salary' : parseFloat(this.sixStep.controls['salary'].value),
            'work_mobile_number' : this.sixStep.controls['work_mobile_number'].value,
            'position' : this.sixStep.controls['position'].value,
            'job_category' : this.sixStep.controls['job_category'].value,
            'internal_phone' : this.sixStep.controls['additionalNumber'].value,
        };
        this.newReq.additionalEmploymentInformationUpdate(id, data).subscribe(
            (data: any) => {
                console.log(data);
                stepper.next();
                text = 'Данные о дополнительной работе сохранены';
                this.showSuccess(text);
                this.spinnerService.hide();
            },
            err => {
                console.log(err);
                this.showError();
                this.spinnerService.hide();
            }
        );
    }

    identityCardUpdate(stepper: MatStepper) {
        let text;
        this.spinnerService.show();

        const id = this.route.snapshot.params['id'];

        const a1 = this.sevenStep.controls['issue_date'].value.substring(0,2);
        const b1 = this.sevenStep.controls['issue_date'].value.substring(2,4);
        const c1 = this.sevenStep.controls['issue_date'].value.substring(4,8);
        const formattedIssueDate = c1 + '-' + b1 + '-' + a1;
        console.log(formattedIssueDate);

        const a2 = this.sevenStep.controls['expire_date'].value.substring(0,2);
        const b2 = this.sevenStep.controls['expire_date'].value.substring(2,4);
        const c2 = this.sevenStep.controls['expire_date'].value.substring(4,8);
        const formattedExpireDate = c2 + '-' + b2 + '-' + a2;
        console.log(formattedExpireDate);
        

        const data = {
            'number' : this.sevenStep.controls['number'].value,
            'issue_date' : formattedIssueDate,
            'expire_date' : formattedExpireDate,
            'issuer' : this.sevenStep.controls['issuer'].value,
            //'docIssuers_other': issTypeNum,
            'place_of_birth': this.sevenStep.controls['place_of_birth'].value,
            'id_card_type': this.sevenStep.controls['id_card_type'].value,
        };
        this.newReq.identityCardUpdate(id, data).subscribe(
            (data: any) => {
                console.log(data);
                stepper.next();
                text = 'Данные о документе обновлены';
                this.showSuccess(text);
                this.spinnerService.hide();
            },
            err => {
                console.log(err);
                this.showError();
                this.spinnerService.hide();
            }
        );
    }

    metaInformationUpdate(stepper: MatStepper) {
        let text;
        this.spinnerService.show();
        let loan_purpose;
        let channel;
        let gcvp_category_selected;
        let channelName

        if (this.eightStep.controls['loan_purpose'].value === 21) {
          loan_purpose = this.eightStep.controls['loan_purpose_other'].value;
        } else {
          loan_purpose = this.eightStep.controls['loan_purpose'].value;
        }

        if (this.eightStep.controls['channel'].value === 39) {
          channel = this.eightStep.controls['channel_other'].value;
        } else {
          let channelId = this.eightStep.controls['channel'].value
          channelName  = this.channelsList.filter(x => x.id === channelId);
          channel = channelName[0].description
          console.log(channel)
        }

        if (this.eightStep.controls['gcvp_type'].value === true) {
          gcvp_category_selected = this.eightStep.controls['gcvp_category'].value;
        } else {
          gcvp_category_selected = '';
        }



      const id = this.route.snapshot.params['id'];
        const data = {
            'loan_purpose_id' :  this.eightStep.controls['loan_purpose'].value,
            'loan_purpose' : 'Потребительские цели',
            'channel' : channel,
            'channel_id' : this.eightStep.controls['channel'].value,
            'children_15' : this.eightStep.controls['children_15'].value,
            'children_15_21' : this.eightStep.controls['children_15_21'].value,
            'additional_income' : parseInt(this.eightStep.controls['additional_income'].value),
            'expenses' : parseInt(this.eightStep.controls['expenses'].value),
            'is_car_owner' : this.eightStep.controls['is_car_owner'].value,
            'education' : this.eightStep.controls['education'].value,
            'family_status' : this.eightStep.controls['family_status'].value,
            'has_not_gcvp' : this.eightStep.controls['gcvp_type'].value,
            'has_pkb_debt': this.eightStep.controls['pkb_debt'].value,
            'gcvp_category' : gcvp_category_selected,
        };
        this.newReq.metaInformationUpdate(id, data).subscribe(
            (data: any) => {
                console.log(data);
                stepper.next();
                text = 'Дополнительная информация сохранена';
                this.showSuccess(text);
                this.spinnerService.hide();
            },
            err => {
                console.log(err);
                this.showError();
                this.spinnerService.hide();
            }
        );
    }

    spouseUpdate(stepper: MatStepper) {
        let text;
        this.spinnerService.show();
        const id = this.route.snapshot.params['id'];
        const data = {
            'first_name' : this.nineStep.controls['first_name'].value,
            'last_name' : this.nineStep.controls['last_name'].value,
            'patronymic_name' : this.nineStep.controls['patronymic_name'].value,
            'is_employed' : this.nineStep.controls['is_employed'].value,
            'work_mobile_number' : this.nineStep.controls['work_mobile_number'].value,
            'organization_name' : this.nineStep.controls['organization_name'].value,
            'organization_address' : this.nineStep.controls['organization_address'].value,
            'position' : this.nineStep.controls['position'].value,
            'salary' : parseFloat(this.nineStep.controls['salary'].value),
        };

        this.newReq.spouseUpdate(id, data).subscribe(
            (data: any) => {
                console.log(data);
                stepper.next();
                text = 'Данные о супруге сохранены';
                this.showSuccess(text);
                this.spinnerService.hide();
            },
            err => {
                console.log(err);
                this.showError();
                this.spinnerService.hide();
            }
        );
    }

    registrationAddressUpdate(stepper: MatStepper) {
        this.spinnerService.show();
        let text;
        const id = this.route.snapshot.params['id'];
        let locality;
        if (this.tenStep.controls['cityReg'].value === undefined) {
          locality = '';
        } else {
          locality  = this.tenStep.controls['cityReg'].value;
        }
        const data = {
            'street' : this.tenStep.controls['streetReg'].value,
            'house' : this.tenStep.controls['homeReg'].value,
            'apartment' : this.tenStep.controls['apartmentReg'].value,
            'index' : this.tenStep.controls['index'].value,
            'country' : this.tenStep.controls['countryReg'].value,
            'region' : this.tenStep.controls['regionReg'].value,
            'area' : this.tenStep.controls['areaReg'].value,
            'locality' : locality,
            'ownership' : this.tenStep.controls['affiliationReg'].value,
        };
        this.newReq.registrationAddressUpdate(id, data).subscribe(
            (data: any) => {
                console.log(data);
                stepper.next();
                text = 'Адрес регистрации сохранен';
                this.showSuccess(text);
                this.spinnerService.hide();
            },
            err => {
                console.log(err);
                this.showError();
                this.spinnerService.hide();
            }
        );
    }

    residentalAddressUpdate(stepper: MatStepper) {
      let locality;
        if (this.elevenStep.controls['cityLive'].value === undefined) {
          locality = '';
        } else {
          locality  = this.elevenStep.controls['cityLive'].value;
        }
        this.spinnerService.show();
        let text;
        const id = this.route.snapshot.params['id'];
        const data = {
            'street' : this.elevenStep.controls['streetLive'].value,
            'house' : this.elevenStep.controls['homeLive'].value,
            'apartment' : this.elevenStep.controls['apartmentLive'].value,
            'index' : this.elevenStep.controls['index'].value,
            'country' : this.elevenStep.controls['countryLive'].value,
            'region' : this.elevenStep.controls['regionLive'].value,
            'area' : this.elevenStep.controls['areaLive'].value,
            'locality' : locality,
            'ownership' : this.elevenStep.controls['affiliationLive'].value,
        };

        this.newReq.residentalAddressUpdate(id, data).subscribe(
            (data: any) => {
                console.log(data);
                stepper.next();
                const text = 'Адрес проживания сохранен';
                this.showSuccess(text);
                this.spinnerService.hide();
                
            },
            err => {
                console.log(err);
                this.showError();
                this.spinnerService.hide();
            }
        );
    }

    inputChange(){
      const a2 = this.sevenStep.controls['expire_date'].value.substring(0,2);
      const b2 = this.sevenStep.controls['expire_date'].value.substring(2,4);
      const c2 = this.sevenStep.controls['expire_date'].value.substring(4,8);
      const formattedExpireDate = a2 + '.' + b2 + '.' + c2;
      console.log(formattedExpireDate);
      const splitDate = formattedExpireDate.split('.');
                const otherDate = splitDate[2] + '-' + splitDate[1] + '-' + splitDate[0];
                const todayDate = moment(new Date()).format('YYYY-MM-DD');
                let diff = ((new Date(otherDate)).getTime() - (new Date(todayDate)).getTime());
                const hours = Math.floor(diff / (1000 * 60 * 60));
                diff -= hours * (1000 * 60 * 60);
                this.diffHour = hours;
                if(this.diffHour < 720){
                  const text = "Срок действия документа истек";
                  this.showErrorr(text);
                  this.inputDis = true;
                } else{
                  this.inputDis = false;
                }
    }
    getRequestInfo() {
        this.spinnerService.show();
        const id = this.route.snapshot.params['id'];
        this.newReq.getRequest(id).subscribe(
            (data: any) => {
                console.log(data);
                const a = data.person.birth_date.split('-')[0];
                const b = data.person.birth_date.split('-')[1];
                const c = data.person.birth_date.split('-')[2];
                const value = c + '.' + b + '.' + a;
                console.log(value);
                const a1 = data.identity_card.issue_date.split('-')[0];
                const b1 = data.identity_card.issue_date.split('-')[1];
                const c1= data.identity_card.issue_date.split('-')[2];
                const value1 = c1 + '.' + b1 + '.' + a1;
                console.log(value1);

                const a2 = data.identity_card.expire_date.split('-')[0];
                const b2 = data.identity_card.expire_date.split('-')[1];
                const c2= data.identity_card.expire_date.split('-')[2];
                const value2 = c2 + '.' + b2 + '.' + a2;
                console.log(value2);
                const splitDate = value2.split('.');
                const otherDate = splitDate[2] + '-' + splitDate[1] + '-' + splitDate[0];
                const todayDate = moment(new Date()).format('YYYY-MM-DD');
                let diff = ((new Date(otherDate)).getTime() - (new Date(todayDate)).getTime());
                const hours = Math.floor(diff / (1000 * 60 * 60));
                diff -= hours * (1000 * 60 * 60);
                this.diffHour = hours;

                this.productTypeLoaded = data.product_type.id;
                console.log(this.productTypeLoaded);
                this.getDocsList();
                // this.productTypeLoaded = data.product_type.id;
                this.secondStep.patchValue({
                    name: 'first_name' in data.person ? data.person.first_name : '',
                    surname: data.person.last_name,
                    patronymic: data.person.patronymic_name,
                    userDateOfb: value,
                    male: data.person.gender.id,
                    clientCitizen: data.person.citizenship,
                });

                this.thirdStep.patchValue({
                    additional_number: data.contact_info.additional_number,
                    home_number: data.contact_info.home_number,
                    email: data.contact_info.email,
                });
                this.fourStep.patchValue({
                  first_name: data.contact_info.contact_persons[0].first_name,
                  last_name: data.contact_info.contact_persons[0].last_name,
                  patronymic_name: data.contact_info.contact_persons[0].patronymic_name,
                  relation_type: (data.contact_info.contact_persons[0].relation_type === null || data.contact_info.contact_persons[0].relation_type_id) ? '' : data.contact_info.contact_persons[0].relation_type_id.id,
                  mobile_number: data.contact_info.contact_persons[0].mobile_number,
              });
              this.twelveStep.patchValue({
                first_name: data.contact_info.contact_persons[1].first_name,
                last_name: data.contact_info.contact_persons[1].last_name,
                patronymic_name: data.contact_info.contact_persons[1].patronymic_name,
                relation_type: (data.contact_info.contact_persons[1].relation_type === null || data.contact_info.contact_persons[1].relation_type_id) ? '' : data.contact_info.contact_persons[1].relation_type_id.id,
                mobile_number: data.contact_info.contact_persons[1].mobile_number,
            });
                this.fiveStep.patchValue({
                    organization_name: data.employment_information.organization_name,
                    organization_address: data.employment_information.organization_address,
                    total_work_exp: data.employment_information.total_work_exp,
                    current_work_exp: data.employment_information.current_work_exp,
                    salary: data.employment_information.salary,
                    work_mobile_number: data.employment_information.work_mobile_number,
                    additionalNumber: data.employment_information.internal_phone,
                    position: data.employment_information.position,
                    job_category: (data.employment_information.job_category === null) ? '' : data.employment_information.job_category.id,
                    additionalWork: 'false'
                });
                this.sixStep.patchValue({
                    organization_name: data.additional_employment.organization_name,
                    organization_address: data.additional_employment.organization_address,
                    total_work_exp: data.additional_employment.total_work_exp,
                    current_work_exp: data.additional_employment.current_work_exp,
                    salary: data.additional_employment.salary,
                    work_mobile_number: data.additional_employment.work_mobile_number,
                    additionalNumber: data.additional_employment.internal_phone,
                    position: data.additional_employment.position,
                    job_category: (data.additional_employment.job_category === null) ? '' : data.employment_information.job_category.id,
                });
                this.sevenStep.patchValue({
                    place_of_birth: data.identity_card.place_of_birth,
                    id_card_type: (data.identity_card.id_card_type === null) ? '' : data.identity_card.id_card_type.id,
                    number: data.identity_card.number,
                    issue_date: value1,
                    expire_date: value2,
                    issuer: (data.identity_card.issuer === null) ? '' : data.identity_card.issuer.id
                });
                this.eightStep.patchValue({
                    loan_purpose: data.meta_information.loan_purpose_id.id,
                    channel: (data.meta_information.channel === null || data.meta_information.channel_id === null) ? '' : data.meta_information.channel_id.id,
                    children_15: data.meta_information.children_15,
                    children_15_21: data.meta_information.children_15_21,
                    additional_income: data.meta_information.additional_income,
                    expenses: data.meta_information.expenses,
                    is_car_owner: data.meta_information.is_car_owner,
                    education: (data.meta_information.education === null) ? '' : data.meta_information.education.id,
                    family_status: (data.meta_information.family_status === null) ? '' : data.meta_information.family_status.id,
                    gcvp_type: false,
                    pkb_debt: data.meta_information.has_pkb_debt,
                });
                this.nineStep.patchValue({
                    first_name: data.spouse.first_name,
                    last_name: data.spouse.last_name,
                    patronymic_name: data.spouse.patronymic_name,
                    is_employed: data.spouse.is_employed,
                    work_mobile_number: data.spouse.work_mobile_number,
                    organization_name: data.spouse.organization_name,
                    organization_address: data.spouse.organization_address,
                    position: data.spouse.position,
                    salary: data.spouse.salary,
                });
                this.tenStep.patchValue({
                    countryReg: (data.registration_adress.country === null) ? '' : data.registration_adress.country.id,
                    regionReg: (data.registration_adress.region === null) ? '' : data.registration_adress.region.id,
                    areaReg: (data.registration_adress.area === null) ? '' : data.registration_adress.area.id,
                    cityReg: (data.registration_adress.locality === null) ? '' : data.registration_adress.locality.id,
                    streetReg: data.registration_adress.street,
                    homeReg: data.registration_adress.house,
                    apartmentReg: data.registration_adress.apartment,
                    index: data.registration_adress.index,
                    affiliationReg: (data.registration_adress.ownership === null) ? '' : data.registration_adress.ownership.id,
                });
                this.elevenStep.patchValue({
                    countryLive: (data.residental_adress.country === null) ? '' : data.residental_adress.country.id,
                    regionLive:  (data.residental_adress.region === null) ? '' : data.residental_adress.region.id,
                    areaLive:  (data.residental_adress.area === null) ? '' : data.residental_adress.area.id,
                    cityLive: (data.residental_adress.locality === null) ? '' : data.residental_adress.locality.id,
                    streetLive: data.residental_adress.street,
                    homeLive: data.residental_adress.house,
                    apartmentLive: data.residental_adress.apartment,
                    index: data.residental_adress.index,
                    affiliationLive: (data.residental_adress.ownership === null) ? '' : data.residental_adress.ownership.id,
                });
                this.spinnerService.hide();
                
            },
            err => {
                console.log(err);
            }
        );
    }

    createRequest() {
      const id = this.route.snapshot.params['id'];
      const data = {
        'files' : this.docs,
        'product_type' : this.firstFormGroup.controls['productType'].value,

        'bid_amount' : this.creditAmount,
        'term_application' :  this.creditTerm,
        'number_dependents' : this.secondFormGroup.controls['dependentsAmount'].value,
        'registration_street' : this.secondFormGroup.controls['streetReg'].value,
        'registration_house' : this.secondFormGroup.controls['homeReg'].value,
        'registration_apartment' : this.secondFormGroup.controls['apartmentReg'].value,
        'residental_street' : this.streetLive,
        'residental_house' : this.homeLive,
        'residental_apartment' : this.apartmentLive,
        'work_place' : this.secondFormGroup.controls['workPlace'].value,
        'position' : this.secondFormGroup.controls['position'].value,
        'home_number' : this.secondFormGroup.controls['homePhone'].value,
        'email' : this.secondFormGroup.controls['email'].value,
        'last_residence_period' : this.secondFormGroup.controls['livePeriod'].value,
        'last_work_period' : this.secondFormGroup.controls['workPeriod'].value,
        'work_experience' : this.secondFormGroup.controls['experiencePeriod'].value,
        'monthly_expances' : this.secondFormGroup.controls['monthlyExpances'].value,
        'acquisition_channel' : this.secondFormGroup.controls['channelType'].value,
        'redemption_method' :  this.firstFormGroup.controls['redemptionType'].value,
        'marial_status' : this.secondFormGroup.controls['marialStatus'].value,
        'education' : this.secondFormGroup.controls['education'].value,
        'registration_country' : this.secondFormGroup.controls['countryReg'].value,
        'registration_region' : this.secondFormGroup.controls['regionReg'].value,
        'registration_area' : this.secondFormGroup.controls['areaReg'].value,
        'registration_locality' : '',
        'residental_country' : this.countryLive,
        'residental_region' : this.regionLive,
        'residental_area' : this.areaLive,
        'residental_locality' : '',
        'job_category' : this.secondFormGroup.controls['jobCategory'].value,
        'credit_goal' : this.secondFormGroup.controls['creditGoal'].value
      };

      this.request.updateRequest(id, data).subscribe((data: any) => {
        console.log(data);
        this.router.navigate(['manager/request-list']);
        this.openSnackBar('Анкета заполнена', 'OK');
      });

    }
    getStorageData() {
        this.localStorage.getItem('clientFio').subscribe((data: any) => {
            const fioSelect = data.split(' ');
            this.userFio = data;
            this.userLastName = fioSelect[0];
            this.userName = fioSelect[1];
            this.userFatherName = fioSelect[2];
            console.log(data);
            console.log(this.userLastName, this.userFatherName, this.userName);
        });

        this.localStorage.getItem('clientDateOFB').subscribe((data) => {
            console.log(data);
            let timestamp;
            timestamp = data;
            const date = new Date(timestamp * 60 * 1000);
            const hours = date.getDate();
            const minutes = date.getMonth();
            const seconds = date.getFullYear();
            console.log(hours);
        });
        this.localStorage.getItem('clientTaxDepName').subscribe((data) => {
            this.userTaxName = data;
            console.log(data);
        });
        this.localStorage.getItem('clientIin').subscribe((data) => {
            this.userIin = data;
            console.log(data);
        });
    }

    preview(files) {
        if (files.length === 0) {
            return;
        }

        const mimeType = files[0].type;

        const reader = new FileReader();
        this.imagePath = files;
        reader.readAsDataURL(files[0]);
        reader.onload = (_event) => {
            this.imgURL = reader.result;
        };
    }

    redemptionChange(id) {
        console.log(id);
    }

    previewTwo(files) {
        if (files.length === 0) {
            return;
        }

        const mimeType = files[0].type;

        const reader = new FileReader();
        this.imagePath = files;
        reader.readAsDataURL(files[0]);
        reader.onload = (_event) => {
            this.imgURLTwo = reader.result;
        };
    }

    previewThree(files) {
        if (files.length === 0) {
            return;
        }

        const mimeType = files[0].type;

        const reader = new FileReader();
        this.imagePath = files;
        reader.readAsDataURL(files[0]);
        reader.onload = (_event) => {
            this.imgURLThree = reader.result;
        };
    }

    getChannelsList() {
      const type = 'acquisitionchannelcatalog';
      this.catalogs.getDynamicCatalog(type).subscribe(
        (data: any) => {
          this.channelsList = data.items;
        }
      );
    }


    checkboxChange(value) {
        console.log(value);
        if (value === true) {
            this.elevenStep.patchValue({
                countryLive: this.tenStep.get('countryReg').value,
                regionLive: this.tenStep.get('regionReg').value,
                areaLive: this.tenStep.get('areaReg').value,
                streetLive: this.tenStep.get('streetReg').value,
                homeLive: this.tenStep.get('homeReg').value,
                apartmentLive: this.tenStep.get('apartmentReg').value,
                affiliationLive: this.tenStep.get('affiliationReg').value,
                cityLive: this.tenStep.get('cityReg').value,
            });
        } else {
            this.fourStep.patchValue({
                countryLive: null,
                regionLive: null,
                areaLive: null,
                streetLive: null,
                homeLive: null,
                apartmentLive: null,
                affiliationLive: null,
                cityLive: null,
            });
        }
    }

    getJobCategory() {
        const type = 'jobcategorycatalog';
        this.catalogs.getDynamicCatalog(type).subscribe(
            (data: any) => {
                this.jobCategoryList = data.items;
            }

        );
    }

    getEducationList() {
        const type = 'educationcatalog';
        this.catalogs.getDynamicCatalog(type).subscribe(
            (data: any) => {
                this.educationList = data.items;
            }
        );
    }

  getGcvpList() {
    const type = 'non_gcvp_persons';
    this.catalogs.getDynamicCatalog(type).subscribe(
      (data: any) => {
        this.gcpvList = data.items;
      }
    );
  }

    getRedemption() {
      const type = 'redemption_method';
      this.catalogs.getDynamicCatalog(type).subscribe(
        (data: any) => {
          this.redemptionList = data.items;
        }
      );
    }

    marialCheck(id) {
        if (id === 25) {
            this.isMarial = true;
        } else {
            this.isMarial = false;
        }
    }

    getMarialStatusList() {
        const type = 'marialstatuscatalog';
        this.catalogs.getDynamicCatalog(type).subscribe(
            (data: any) => {
                this.marialStatusList = data.items;
            }
        );
    }

    getDocTypes() {
        const type = 'id_card_types';
        this.catalogs.getDynamicCatalog(type).subscribe(
            (data: any) => {
                this.cardTypes = data.items;
            }
        );
    }


    getProductType() {
        const type = 'producttype';
        this.catalogs.getCatalog(type).subscribe(
            (data: any) => {
                this.productTypeList = data;
            }
        );
    }

    getCountryList() {
        const type = 'country';
        this.catalogs.getCatalog(type).subscribe(
            (data: any) => {
                this.countryList = data;
            }
        );
    }

    getRegionList(id) {
        console.log('TEST');
        const type = 'region';
        this.catalogs.getRegion(id).subscribe(
            (data: any) => {
                this.regionList = data;
            }
        );
    }

    getLocationList(id) {
      this.catalogs.getCityList(id).subscribe(
        (data: any) => {
          this.cityList = data;
          if (data.length > 0 ) {
            this.cityListStatus = true;
          } else {
            this.cityListStatus = false;
          }
          console.log(this.cityListStatus);
        }
      );
    }

  getLocationListLive(id) {
    this.catalogs.getCityList(id).subscribe(
      (data: any) => {
        this.cityListLive = data;
        if (data.length > 0 ) {
          this.cityLiveListStatus = true;
        } else {
          this.cityLiveListStatus = false;
        }
      }
    );
  }

    getAreaList(id) {
      this.cityListStatus = false;
      console.log(id);
      this.catalogs.getArea(id).subscribe(
        (data: any) => {
          this.areaList = data;
        }
      );
    }

    getLiveCountryList() {
        const type = 'country';
        this.catalogs.getCatalog(type).subscribe(
            (data: any) => {
                this.countryLiveList = data;
            }
        );
    }

    getLiveRegionList(id) {
        console.log('TEST');
        const type = 'region';
        this.catalogs.getRegion(id).subscribe(
            (data: any) => {
                this.regionLiveList = data;
            }
        );
    }

    getLiveAreaList(id) {
        console.log(id);
        this.catalogs.getArea(id).subscribe(
            (data: any) => {
                this.areaLiveList = data;
            }
        );
    }

    getCreditGoalList() {
        const type = 'creditgoalcatalog';
        this.catalogs.getDynamicCatalog(type).subscribe(
            (data: any) => {
              this.creditGoalList = data.items;
              console.log(this.creditGoalList);
            }
        );
    }

    getOwnership() {
        const type = 'ownershipcatalog';
        this.catalogs.getDynamicCatalog(type).subscribe(
            (data: any) => {
                this.ownershipList = data.items;
            }
        );
    }

    getDocRequired() {
        const type = 'documentrequired';
        this.catalogs.getCatalog(type).subscribe(
            (data: any) => {
                this.docReq = data;
            }
        );
    }

    profileNextStep() {
        this.profileStep++;
    }

    sendSms() {
        this.smsSent = true;
    }

    smsBack() {
        this.smsSent = false;
    }

    finish() {
        this.router.navigate(['manager/request-list']);
        this.openSnackBar('Завка создана', 'OK');
    }

    creditCalculate() {
        console.log(this.firstFormGroup.get('creditAmount').value);
        console.log(this.firstFormGroup.get('creditTerm').value);
        let precent;
        precent = parseInt(this.firstFormGroup.get('creditAmount').value) / 100;
        this.totalOverpayment = 11 * precent;
        this.totalCredit = parseInt(this.totalOverpayment) + parseInt(this.firstFormGroup.get('creditAmount').value);
    }

    getDocsList() {
      const id = this.productTypeLoaded;
      console.log(id);
      this.catalogs.getDocsList(id).subscribe(
        (data: any) => {
          this.docList = data;
          this.docTypeId = data.id;
          console.log(data);
        }
      );
    }


    private _filterStates(value: string): string[] {
        console.log(value);
        const filterValue = value.toLowerCase();

        return this.result.filter(option => option.toLowerCase().indexOf(filterValue) === 0);

    }

    fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
  }

    spouseWorkCheck(data) {
      console.log(data, typeof data);
    if (data === 'true') {
    this.isSpouseWork = true;
    } else {
    this.isSpouseWork = false;
    }
    }

    getGender() {
    const type = 'gender';
    this.catalogs.getDynamicCatalog(type).subscribe(
    (data: any) => {
        this.genderList = data.items;
        console.log(data);
    }
    );
    }

    getRelationDegree() {
    const type = 'relationdegreecatalog';
    this.catalogs.getDynamicCatalog(type).subscribe(
    (data: any) => {
        this.relationDegree = data.items;
        console.log(data);
    }
    );
    }

    getIssuers() {
    const type = 'issuers';
    this.catalogs.getDynamicCatalog(type).subscribe(
    (data: any) => {
      this.docIssuers = data.items;
      console.log(data);
    }
    );
    }

    fileEvent(e, name, typeId) {
      this.filedata = e.target.files[0];
      console.log(e);
      this.spinnerService.show();
      const formData = new FormData();
      formData.append('document', this.filedata);
      formData.append('description', name);

      this.http.post(this.apiEndpoint + '/api/v2/sales/application_documents/', formData, )
        .subscribe(
          (data: any) => {
            if (this.docs.files.find(x => x.document_type === typeId)) {
              this.docs.files = this.docs.files.filter(function( obj ) {
                return obj.document_type  !== typeId;
              });
              this.docs.files.push({'document_id': data.id, 'document_type': typeId});
            } else {
              this.docs.files.push({'document_id': data.id, 'document_type': typeId});
            }
            (<HTMLInputElement>document.getElementById(typeId)).value = 'Файл загружен';
            console.log(this.docs);
            this.spinnerService.hide();
            // console.log(typeId);
            // console.log(typeof formData, this.filedata);
            // this.myInputVariable.nativeElement.value = null;
            // console.log(this.filedata);
            // this.docs.files = this.docs.files.map(e => e['document_type'])
            // .map((e, i, final) => final.indexOf(e) === i && i)
            // .filter(e => this.docs.files[e]).map(e => this.docs.files[e]);
            //   console.log(this.docs.files);

          },
          err => {
            console.log(err);
                this.showError();
                this.spinnerService.hide();
          });
    }

    sentDocs(stepper: MatStepper) {
      const id = this.route.snapshot.params['id'];
      if (this.docs.files.length < this.docList.length )  {
        this.showDocError();
      } else {
        this.newReq.sentDocs(id, this.docs).subscribe(
          (data: any) => {
            console.log(data);
            stepper.next();this.router.navigate(['manager/request-list']);
            this.openSnackBar('Анкета заполнена', 'OK');
          }
        );
      }
    }

    docIssuersOtherSelect(id){
      console.log(id);
      if (id === 51) {
        this.docIssuersOther = true;
      } else {
        this.docIssuersOther = false;
      }
    }
    relshipOtherSelect(id) {
        if (id === 9) {
          this.relationshipOther = true;
        } else {
          this.relationshipOther = false;
        }
    }
    secondRelshipOtherSelect(id) {
      if (id === 9) {
        this.secondRelationshipOther = true;
      } else {
        this.secondRelationshipOther = false;
      }
    }
    channelOtherSelect(data) {
      if (data === 39) {
        this.channelOther = true;
      } else {
        this.channelOther = false;
      }
    }
    purposeOtherSelect(id) {
      if (id === 21) {
        this.purposeOther = true;
      } else {
        this.purposeOther = false;
      }
    }

    additionalWorkChange(data) {
      if (data === 'true') {
        this.isAdditionalWork = true;
      } else {
        this.isAdditionalWork = false;
      }
    }

  gcvpChange(data) {
    if (data === true) {
      this.gcvpType = true;
    } else {
      this.gcvpType = false;
    }
    console.log(this.gcvpType);
  }
  pkbChange(data) {
    if (data === true) {
      this.pkbType = true;
    } else {
      this.pkbType = false;
    }
    console.log(this.pkbType);
  }
}
