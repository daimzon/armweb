import { Component, OnInit } from '@angular/core';
import {environment} from '../../../../environments/environment';
import { HttpClient, HttpHeaders,  HttpEventType } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import {MatDialogRef} from '@angular/material';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';

@Component({
  selector: 'app-feedback-form',
  templateUrl: './feedback-form.component.html',
  styleUrls: ['./feedback-form.component.scss']
})
export class FeedbackFormComponent implements OnInit {

  title: any;
  description: any;
  files: any;
  filedata: any;
  apiEndpoint = environment.APIEndpoint;


  constructor(
    private http: HttpClient,
    private toastr: ToastrService,
    public dialogRef: MatDialogRef<FeedbackFormComponent>,
    private spinnerService: Ng4LoadingSpinnerService,


  ) { }

  ngOnInit() {
  }

  showError(text) {
    this.toastr.error(text);
  }

  showSuccess(text) {
    this.toastr.success(text);
  }

  uploadRandom() {

    const formData = new FormData();
    formData.append('image', this.filedata);
    formData.append('title', this.title);
    formData.append('description', this.description);
    formData.append('form_factor', 'Google Chrome');
    this.spinnerService.show();
    this.http.post(this.apiEndpoint + '/api/v2/feedback/', formData, )
      .subscribe(
        (data: any) => {
          console.log(data);
          const text = 'Отчет отправлен';
          this.showSuccess(text)
          this.dialogRef.close();
          this.spinnerService.hide();
        },
        (err: any) => {
          const text = 'Техническая ошибка';
          this.showError(text)
        })
    ;
  }

  fileEvent(e) {
    this.filedata = e.target.files[0];
    console.log(e);
  }

}
