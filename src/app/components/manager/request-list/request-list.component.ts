import {Component, OnInit} from '@angular/core';
import {requestService} from '../../../services/request-list.service';
import {Router, ActivatedRoute} from '@angular/router';
import {LocalStorage} from '@ngx-pwa/local-storage';
import {RequestCommentComponent} from '../request-comment/request-comment.component';
import {RequestAssignComponent} from '../request-assign/request-assign.component';
import {RequestProcessComponent} from '../request-process/request-process.component';
import { MatDialog } from '@angular/material/dialog';
import {catalogService} from '../../../services/catalogs.service';
import { interval } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import {RiskService} from '../../../services/risk.service';
import {saveAs as importedSaveAs} from 'file-saver';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';

@Component({
    selector: 'app-request-list',
    templateUrl: './request-list.component.html',
    styleUrls: ['./request-list.component.scss']
})
export class RequestListComponent implements OnInit {
   public date = { begin: new Date(), end: new Date () };
    requests: any;
    filterType: any;
    creditType: any;
    sotUser: any;
    manager: string;
    statusType: any;
    searchType: any = '';
    searchStatus: any = '';
    userRole: any;
    requestField: any = '';
    fourSm = false;
    showNewButton = false;
    pageCount = 1;
    riskOfficer = false;
    dateRequest = {
      begin: '',
      end: ''
    };
    backAvailable: boolean;
    nextAvailable: boolean;
    constructor(
        public reqService: requestService,
        public router: Router,
        private route: ActivatedRoute,
        protected localStorage: LocalStorage,
        public dialog: MatDialog,
        public catalogs: catalogService,
        private toastr: ToastrService,
        private riskServ: RiskService,
        private spinnerService: Ng4LoadingSpinnerService,
    ) {
    }

    ngOnInit() {
        this.getCreditType();
        this.getStatusType();
        this.testStats();
        this.getUserRole();
        //this.getUsers();
        // this.update();
    }

  showSuccess(id) {
    this.toastr.success(id, 'Заявка инициализируется');
  }

  showUpdateOk(text) {
    this.toastr.success(text);
  }

  showUpdateFalse(text) {
    this.toastr.error(text);
  }

    getCreditType() {
        const type = 'producttype';
        this.catalogs.getCatalog(type).subscribe(
            (data: any) => {
                this.creditType = data;
            },
        );
    }

    // update() {
    //   const secondsCounter = interval(6000);
    //   secondsCounter.subscribe(n =>
    //     this.getUserRole());
    // }

    getUserRole() {
        this.spinnerService.show();
        const role = localStorage.getItem('permissions');
        this.userRole = role.includes('LA');
        console.log('CHECK LIST:' , this.userRole);
        if (role.includes('SM') === true) {
            this.filter();
        } else if (role.includes('LA') === true) {
            this.getLoanList();
        } else if (role.includes('RO') === true) {
            this.getRiskList();
            this.riskOfficer = true;
        }

        if (role.includes('SM04')) {
            this.fourSm = true;
        }

        if (role.includes('SM')) {
          this.showNewButton = true;
        }
    }

    getStatusType() {
        this.catalogs.getStatusList().subscribe(
            (data: any) => {
                this.statusType = data;
            }
        );
    }

    getLoanList() {
        this.reqService.loanAdminList().subscribe(
            (data: any) => {
                this.requests = data.results;

              for (let i = 0; i < data.results.length; i++) {
                if (data.results[i].status === 1) {
                  data.results[i].status_header = 'Ожидает заполнения';
                } else if (data.results[i].status === 2) {
                  data.results[i].status_header = 'В процессе формирования согласия';
                } else if (data.results[i].status === 3) {
                  data.results[i].status_header = 'Согласие сформировано';
                } else if (data.results[i].status === 4) {
                  data.results[i].status_header = 'В работе';
                } else if (data.results[i].status === 5) {
                  data.results[i].status_header = 'В работе риск офицера';
                } else if (data.results[i].status === 0) {
                  data.results[i].status_header = 'Создано';
                } else if (data.results[i].status === 6) {
                  data.results[i].status_header = 'Даны предложения';
                } else if (data.results[i].status === 7) {
                  data.results[i].status_header = 'В процессе формирования договора';
                } else if (data.results[i].status === 8) {
                  data.results[i].status_header = 'Договор сформирован';
                } else if (data.results[i].status === 9) {
                  data.results[i].status_header = 'В работе кредитного администратора';
                } else if (data.results[i].status === 10) {
                  data.results[i].status_header = 'Одобрено кредитным администратором';
                } else if (data.results[i].status === 11) {
                  data.results[i].status_header = 'Редактируется риск офицером';
                } else if (data.results[i].status === 200) {
                  data.results[i].status_header = 'Выдано';
                } else if (data.results[i].status === 500) {
                  data.results[i].status_header = 'Отказано по техническим причинам';
                } else if (data.results[i].status === 501) {
                  data.results[i].status_header = 'Отказано банком';
                } else if (data.results[i].status === 502) {
                  data.results[i].status_header = 'Отказано клиентом';
                } else if (data.results[i].status === 503) {
                  data.results[i].status_header = 'Заявка закрыта в связи открытии новой';
                } else if (data.results[i].status === 504) {
                  data.results[i].status_header = 'Заявка закрыта в связи с бездействием';
                }
              }
              this.spinnerService.hide();
            }
        );
    }

    testStats() {
      this.catalogs.getLastMonthes().subscribe(
        (data: any) => {
          console.log(data);
        }
      );
    }

    filter() {
      let dateBefore;
      let dateAfter;
      console.log(this.requestField);

      const dateBegin = new Date(this.dateRequest.begin);
      dateBefore = moment(dateBegin).format('YYYY-MM-DD');

      const dateEnd = new Date(this.dateRequest.end);
      dateAfter = moment(dateEnd).format('YYYY-MM-DD');
        const params = {
            product_type: this.searchType,
            status: this.searchStatus,
            q: this.requestField,
            date_after: '',
            date_before: '',
            count: this.pageCount
        };

        this.reqService.getRequestListWithParams(params).subscribe(
            (data: any) => {
                this.requests = data.results;
                this.nextAvailable = data.next;
                console.log(this.requests);

                if (this.pageCount > 1) {
                  this.backAvailable = true;
                } else {
                  this.backAvailable = false;
                }

                for (let i = 0; i < data.results.length; i++) {
                    if (data.results[i].status === 1) {
                        data.results[i].status_header = 'Ожидает заполнения';
                    } else if (data.results[i].status === 2) {
                        data.results[i].status_header = 'В процессе формирования согласия';
                    } else if (data.results[i].status === 3) {
                        data.results[i].status_header = 'Согласие сформировано';
                    } else if (data.results[i].status === 4) {
                      data.results[i].status_header = 'В работе';
                    } else if (data.results[i].status === 5) {
                      data.results[i].status_header = 'В работе риск офицера';
                    } else if (data.results[i].status === 0) {
                      data.results[i].status_header = 'Создано';
                    } else if (data.results[i].status === 6) {
                      data.results[i].status_header = 'Даны предложения';
                    } else if (data.results[i].status === 7) {
                      data.results[i].status_header = 'В процессе формирования договора';
                    } else if (data.results[i].status === 8) {
                        data.results[i].status_header = 'Договор сформирован';
                    } else if (data.results[i].status === 9) {
                        data.results[i].status_header = 'В работе кредитного администратора';
                    } else if (data.results[i].status === 10) {
                        data.results[i].status_header = 'Одобрено кредитным администратором';
                    } else if (data.results[i].status === 11) {
                      data.results[i].status_header = 'Редактируется риск офицером';
                    } else if (data.results[i].status === 200) {
                      data.results[i].status_header = 'Выдано';
                    } else if (data.results[i].status === 500) {
                      data.results[i].status_header = 'Отказано по техническим причинам';
                    } else if (data.results[i].status === 501) {
                      data.results[i].status_header = 'Отказано банком';
                    } else if (data.results[i].status === 502) {
                      data.results[i].status_header = 'Отказано клиентом';
                    } else if (data.results[i].status === 503) {
                      data.results[i].status_header = 'Заявка закрыта в связи открытии новой';
                    } else if (data.results[i].status === 504) {
                      data.results[i].status_header = 'Заявка закрыта в связи с бездействием';
                    }
                }
                this.spinnerService.hide();
              const text = 'Список обновлен';
              this.showUpdateOk(text);
            },
          (err: any) => {
            const text = 'Техническая ошибка';
            this.showUpdateFalse(text);
          }
        );
    }

    search() {
      let dateBefore;
      let dateAfter;
      console.log(this.requestField);

      const dateBegin = new Date(this.dateRequest.begin);
      dateBefore = moment(dateBegin).format('YYYY-MM-DD');

      const dateEnd = new Date(this.dateRequest.end);
      dateAfter = moment(dateEnd).format('YYYY-MM-DD');
        const params = {
            product_type: this.searchType,
            status: this.searchStatus,
            q: this.requestField,
            date_after: '',
            date_before: '',
            count: this.pageCount
        };

        this.reqService.filterListWithParams(params).subscribe(
            (data: any) => {
                this.requests = data.results;
                this.nextAvailable = data.next;
                console.log(this.requests);

                if (this.pageCount > 1) {
                  this.backAvailable = true;
                } else {
                  this.backAvailable = false;
                }

                for (let i = 0; i < data.results.length; i++) {
                    if (data.results[i].status === 1) {
                        data.results[i].status_header = 'Ожидает заполнения';
                    } else if (data.results[i].status === 2) {
                        data.results[i].status_header = 'В процессе формирования согласия';
                    } else if (data.results[i].status === 3) {
                        data.results[i].status_header = 'Согласие сформировано';
                    } else if (data.results[i].status === 4) {
                      data.results[i].status_header = 'В работе';
                    } else if (data.results[i].status === 5) {
                      data.results[i].status_header = 'В работе риск офицера';
                    } else if (data.results[i].status === 0) {
                      data.results[i].status_header = 'Создано';
                    } else if (data.results[i].status === 6) {
                      data.results[i].status_header = 'Даны предложения';
                    } else if (data.results[i].status === 7) {
                      data.results[i].status_header = 'В процессе формирования договора';
                    } else if (data.results[i].status === 8) {
                        data.results[i].status_header = 'Договор сформирован';
                    } else if (data.results[i].status === 9) {
                        data.results[i].status_header = 'В работе кредитного администратора';
                    } else if (data.results[i].status === 10) {
                        data.results[i].status_header = 'Одобрено кредитным администратором';
                    } else if (data.results[i].status === 11) {
                      data.results[i].status_header = 'Редактируется риск офицером';
                    } else if (data.results[i].status === 200) {
                      data.results[i].status_header = 'Выдано';
                    } else if (data.results[i].status === 500) {
                      data.results[i].status_header = 'Отказано по техническим причинам';
                    } else if (data.results[i].status === 501) {
                      data.results[i].status_header = 'Отказано банком';
                    } else if (data.results[i].status === 502) {
                      data.results[i].status_header = 'Отказано клиентом';
                    } else if (data.results[i].status === 503) {
                      data.results[i].status_header = 'Заявка закрыта в связи открытии новой';
                    } else if (data.results[i].status === 504) {
                      data.results[i].status_header = 'Заявка закрыта в связи с бездействием';
                    }
                }
                this.spinnerService.hide();
              const text = 'Список обновлен';
              this.showUpdateOk(text);
            },
          (err: any) => {
            const text = 'Техническая ошибка';
            this.showUpdateFalse(text);
          }
        );
    }
    getRiskList() {

      const params = {
        product_type: this.searchType,
        status: '',
        q: this.requestField,
        date_after: '',
        date_before: '',
        count: this.pageCount
      };

      const data = {
        first : 11,
        second: 5
      }

      this.riskServ.getList(params, data).subscribe(
        (data: any) => {
          this.requests = data.results;
          this.nextAvailable = data.next;

          if (this.pageCount > 1) {
            this.backAvailable = true;
          } else {
            this.backAvailable = false;
          }
          this.requests = data.results;
          for (let i = 0; i < data.results.length; i++) {
            if (data.results[i].status === 1) {
              data.results[i].status_header = 'Ожидает заполнения';
            } else if (data.results[i].status === 2) {
              data.results[i].status_header = 'В процессе формирования согласия';
            } else if (data.results[i].status === 3) {
              data.results[i].status_header = 'Согласие сформировано';
            } else if (data.results[i].status === 4) {
              data.results[i].status_header = 'В работе';
            } else if (data.results[i].status === 5) {
              data.results[i].status_header = 'В работе риск офицера';
            } else if (data.results[i].status === 0) {
              data.results[i].status_header = 'Создано';
            } else if (data.results[i].status === 6) {
              data.results[i].status_header = 'Даны предложения';
            } else if (data.results[i].status === 7) {
              data.results[i].status_header = 'В процессе формирования договора';
            } else if (data.results[i].status === 8) {
              data.results[i].status_header = 'Договор сформирован';
            } else if (data.results[i].status === 9) {
              data.results[i].status_header = 'В работе кредитного администратора';
            } else if (data.results[i].status === 10) {
              data.results[i].status_header = 'Одобрено кредитным администратором';
            } else if (data.results[i].status === 11) {
              data.results[i].status_header = 'Редактируется риск офицером';
            }  else if (data.results[i].status === 200) {
              data.results[i].status_header = 'Выдано';
            } else if (data.results[i].status === 500) {
              data.results[i].status_header = 'Отказано по техническим причинам';
            } else if (data.results[i].status === 501) {
              data.results[i].status_header = 'Отказано банком';
            } else if (data.results[i].status === 502) {
              data.results[i].status_header = 'Отказано клиентом';
            } else if (data.results[i].status === 503) {
              data.results[i].status_header = 'Заявка закрыта в связи открытии новой';
            } else if (data.results[i].status === 504) {
              data.results[i].status_header = 'Заявка закрыта в связи с бездействием';
            }
          }
          const text = 'Список обновлен';
          this.showUpdateOk(text);
        },
        (err: any) => {
          const text = 'Техническая ошибка';
          this.showUpdateFalse(text);
        }
      );
    }


  getList() {
        this.reqService.getRequestList().subscribe(
            (data: any) => {
                this.requests = data.results;
                console.log(this.requests);
                for (let i = 0; i < data.results.length; i++) {
                    if (data.results[i].status === 1) {
                        data.results[i].status_header = 'Одобрено';
                    } else if (data.results[i].status === 2) {
                        data.results[i].status_header = 'Отказано';
                    } else if (data.results[i].status === 3) {
                        data.results[i].status_header = 'В процессе';
                    }
                }
            });
    }

    getStats() {
        this.reqService.getStats().subscribe(
            (data: any) => {
                console.log(data);
                importedSaveAs(data, 'report.xlsx');
            });
    }


    goToRequest(id, readable_id, status) {
      if (status === 1) {
        this.router.navigate(['manager/new-profile', id]);
      } else if (status === 0) {
        this.router.navigate(['manager/request-view', id]);
        this.showSuccess(readable_id);
      } else {
        this.router.navigate(['manager/request-view', id]);
      }
    }

    goToNewRequest() {
        this.router.navigate(['manager/new-request']);
    }

    nextPage() {
        this.pageCount = this.pageCount + 1;
        this.filter();
    }

    previosPage() {
        this.pageCount = this.pageCount - 1;
        this.filter();
    }
}
