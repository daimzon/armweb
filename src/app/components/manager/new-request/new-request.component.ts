import { Component, OnInit } from '@angular/core';
import {iinService} from '../../../services/iin-check.servise';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, CanActivate } from '@angular/router';
import {requestService} from '../../../services/request-list.service';
import {catalogService} from '../../../services/catalogs.service';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-new-request',
    templateUrl: './new-request.component.html',
    styleUrls: ['./new-request.component.scss']
})

export class NewRequestComponent implements OnInit {
    public iin: any;
    public success = false;
    public clientData: any;
    public iinError = false;
    public bidError = false;
    public phoneError = false;
    productTypeList: any;
    redemptionList: any;
    allert = false;
      maxAount: any;
      maxTerm: any;
      minAmont: any;
      minTerm: any;

    productType: any;
    redemptionType: any;
    maturityDate: any;
    Commission: any;
    creditTerm: any;

  public nextStep = false;
    public firstStep = true;
    creditAmmount: any;
    constructor(
        public iinCheck: iinService,
        private spinnerService: Ng4LoadingSpinnerService,
        protected localStorage: LocalStorage,
        private snackBar: MatSnackBar,
        private router: Router,
        public reqService: requestService,
        public catalogServ: catalogService,
        private toastr: ToastrService
    ) { }

    iinInput: number;
    phoneInput: number;
    bidInput: number;
    iinSuccess = false;

    ngOnInit() {
      this.getProductType();
      this.getRedemption();
    }

    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
            duration: 2000,
        });
    }

    delErr() {
        this.iinError = false;
        this.bidError = false;
        this.phoneError = false;
    }

    init() {
      console.log(this.productType)
      let text;
      const data = {
        'iin': this.iinInput,
        'mobile_number' : this.phoneInput,
        'loan_amount' : this.bidInput,
        'product_type' : this.productType,
        'redemption_method' : this.redemptionType,
        'planned_payment_date' : this.maturityDate,
        'is_commission' : this.Commission,
        'term' : this.creditTerm,
        'is_visual_check_failed' : this.allert
      };
      console.log(typeof this.maturityDate);
      if (this.iinInput === undefined) {
        this.iinError = true;
        text = 'Введите иин';
        this.showError(text);
      } else if (!this.iinValidation(this.iinInput) || this.iinInput.toString().length < 12) {
        this.iinError = true;
        text = 'ИИН не верный';
        this.showError(text);
      } else if (this.productType === undefined) {
        text = 'Продукт не выбран';
        this.showError(text);
      } else if (this.bidInput > this.maxAount) {
        text = 'Сумма кредита больше возможной';
        this.showError(text);
      } else if (this.bidInput < this.minAmont) {
        text = 'Сумма кредита меньше минимальной';
        this.showError(text);
      } else if (this.creditTerm > this.maxTerm) {
        text = 'Срок кредита больше возможного';
        this.showError(text);
      } else if (this.bidInput === undefined) {
        text = 'Введите сумму';
        this.showError(text);
      } else if (this.creditTerm === undefined) {
        text = 'Введите срок';
        this.showError(text);
      } else if (this.phoneInput === undefined) {
        text = 'Введите номер телефона';
        this.showError(text);
      }  else if (this.creditTerm < this.minTerm) {
        text = 'Срок кредита меньше возможного';
        this.showError(text);
      } else if (this.maturityDate === undefined) {
        text = 'Введите дату погашения';
        this.showError(text);
      } else if (parseInt( this.maturityDate) > 31 || parseInt(this.maturityDate) < 1) {
        text = 'Неверная дата погашения';
        this.showError(text);
      } else {
        this.spinnerService.show();
        this.reqService.initRequest(data).subscribe(
          data => {
            this.spinnerService.hide();
            this.router.navigate(['manager/request-list']);
          },
          err => {
            text = 'Техническая ошибка';
            this.showError(text);
          }
          );
      }
    }

  showError(text) {
    this.toastr.error(text);
  }

    sendIin(iinValue) {
        this.spinnerService.show();
        this.iinCheck.getUsrInfo(iinValue).subscribe(
            data => {
                this.clientData = data;
                console.log('data from Altyn: ', data);
                if (this.clientData.isResident > 0) {
                  // this.router.navigate(['new-profile']);
                  this.success = true;
                }

                this.localStorage.setItem('clientFio', this.clientData.fio).subscribe(() => {});
                this.localStorage.setItem('clientDateOFB', this.clientData.dateOFB).subscribe(() => {});
                this.localStorage.setItem('clientTaxDepName', this.clientData.dateOFB).subscribe(() => {});
                this.localStorage.setItem('clientIin', iinValue).subscribe(() => {});
                this.spinnerService.hide();
            },
            err => {
                this.spinnerService.hide();
                this.iinError = true;
            });
    }

    iinValidation(iinObject) {
        const iin = iinObject.toString(),
            nn = iin.split('');
        let s = 0;
        for (let i = 0; i < 11; i++) {
            s = s + (i + 1) * nn[i];
        }
        let k = s % 11;
        if (k === 10) {
            s = 0;
            for (let i = 0; i < 11; i++) {
                let t = (i + 3) % 11;
                if (t === 0) {
                    t = 11;
                }
                s = s + t * nn[i];
            }
            k = s % 11;
            if (k === 10) {
                return false;
            }
            return k == iin.substr(11, 1);
        }
        return k == iin.substr(11, 1);
    }




    checkIin() {
        let text;
        const iinValue = this.iinInput;
        if (!this.iinValidation(iinValue) || iinValue.toString().length < 12) {
            this.iinError = true;
            text = 'ИИН не верный';
            this.showError(text);
        } else {
            this.init();
        }
    }

    checkNumber() {
        this.success = false;
        this.nextStep = true;
    }

    amountChange(newValue) {
        this.creditAmmount = newValue;
        this.localStorage.setItem('credit_amount', newValue).subscribe(() => {});
    }

    termChange(newValue) {
        this.creditTerm = newValue;
        this.localStorage.setItem('credit_term', newValue).subscribe(() => {});
    }

    getProductType() {
      const type = 'producttype';
      this.catalogServ.getCatalog(type).subscribe(
        (data: any) => {
          this.productTypeList = data;
        }
      );
    }

    getRedemption() {
      let selected;
      const type = 'redemption_method';
      this.catalogServ.getDynamicCatalog(type).subscribe(
        (data: any) => {
          this.redemptionList = data.items;
          selected = this.redemptionList.find(x => x.is_default === true);
          this.redemptionType = selected.id;
          this.Commission = 'true';
        }
      );
    }

    productChange(data) {
        const product = this.productTypeList.find(x => x.id === data);
        console.log(product);
        this.maxAount = product.max_amount;
        this.maxTerm = product.max_term;
        this.minAmont = product.min_amount;
        this.minTerm = product.min_term;
        this.productType = product.id;
    }

    commissionChange(data) {
        console.log(data);
    }

}
