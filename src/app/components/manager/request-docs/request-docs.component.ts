import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-request-docs',
  templateUrl: './request-docs.component.html',
  styleUrls: ['./request-docs.component.scss']
})
export class RequestDocsComponent implements OnInit {
  elementType : 'url' | 'canvas' | 'img' = 'url';
  value : string = 'Za monilit';
  constructor() { }

  ngOnInit() {
  }

}
