import { Component, OnInit, Inject } from '@angular/core';
import {WebCamComponent} from 'ack-angular-webcam';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { ToastrService } from 'ngx-toastr';

export interface DialogData {
  typeId: string;
  name: string;
}
@Component({
    selector: 'app-new-photo',
    templateUrl: './new-photo.component.html',
    styleUrls: ['./new-photo.component.scss']
})
export class NewPhotoComponent implements OnInit {
  apiEndpoint = environment.APIEndpoint;
  webcam: WebCamComponent;
  base64;
  fileFormData: any;
  closedData: any;
  options: {
    width: '100vw',
    height: '100vw'
  };
  userImage: any;
    constructor(
      public dialogRef: MatDialogRef<NewPhotoComponent>,
      @Inject(MAT_DIALOG_DATA)
      public data: DialogData,
      private http: HttpClient,
      private toastr: ToastrService

    ) {
      console.log(this.data);
    }

    ngOnInit() {
    }

  showError(text) {
    this.toastr.error(text);
  }

  showSuccess(text) {
    this.toastr.success(text);
  }

  genBase64( webcam: WebCamComponent ) {
    webcam.getBase64()
      .then( base => this.base64 = base)
      .catch( e => console.error(e) );
    // let fileOfBlob;
    // fileOfBlob = new Blob([this.base64], {type: 'image/jpg'});
    // this.fileFormData = new File([this.base64], 'selfi.jpg');
  }

  b64toBlob(dataURI) {
    const byteString = atob(dataURI.split(',')[1]);
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);

    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    let tempBlob =  new Blob([ab], { type: 'image/jpg' });
    return new File([tempBlob], 'selfi.jpg');
  }

  postFormData() {
    this.fileFormData = this.b64toBlob(this.base64)
    const formData = new FormData();
    formData.append('document', this.fileFormData);
    formData.append('description', this.data.name);
    this.http.post(this.apiEndpoint + '/api/v2/sales/application_documents/', formData, )
      .subscribe(
        (response: any) => {
          console.log(response);
          this.closedData = {
            typeID: response.doc_type,
            id: response.id
          };
          this.dialogRef.close({data: this.closedData});
        });
  }

  }
