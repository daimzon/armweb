import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoneRequestListComponent } from './request-list.component';

describe('RequestListComponent', () => {
  let component: DoneRequestListComponent;
  let fixture: ComponentFixture<DoneRequestListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoneRequestListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoneRequestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
