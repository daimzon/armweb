import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {CreditCalcComponent} from '../manager/credit-calc/credit-calc.component';
import {FeedbackFormComponent} from '../manager/feedback-form/feedback-form.component';

import { RouterModule, Router } from '@angular/router';
import {interval} from 'rxjs';
@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
    userRole: any;
    firstName: any;
    lastName: any;
    opRole: any;
    loanRole: any;
    fourSm = false;
    riskOfficer: any;
    rRole: any;
    constructor(
        private translate: TranslateService,
        public dialog: MatDialog,
        public router: Router
    ) { }

    openCalc(): void {
        const dialogRef = this.dialog.open(CreditCalcComponent, {
            panelClass: 'calcDialog',
            width: '1200px'
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    openFeedback(): void {
        const dialogRef = this.dialog.open(FeedbackFormComponent, {
            panelClass: 'calcDialog',
            width: '600px'
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    ngOnInit() {
      this.update()
      this.getUserName()
    }

    update() {
      const secondsCounter = interval(3000);
      secondsCounter.subscribe(n =>
        this.getUserRole());
        this.getUserName()
    }

    getUserName() {
      this.firstName = localStorage.getItem('firstName');
      this.lastName = localStorage.getItem('lastName');
    }

    getUserRole() {
      const role = localStorage.getItem('permissions');
      if (role != null) {
          this.userRole = role.includes("SM");
        this.loanRole = role.includes("LA");
        this.opRole = role.includes("OP");
        this.rRole = role.includes("RO");
      }
      if (role.includes('SM04') || role.includes('RO') || role.includes('LA') ) {
        this.fourSm = true;
    }
    }

    goToRequest(type) {
        this.router.navigate(['manager/request-list', type]);
    }

    goToNewRequest() {
        this.router.navigate(['manager/new-request']);
    }

    goToNotifications() {
        this.router.navigate(['manager/notifications']);
    }

}
