import { Injectable } from '@angular/core';

import { mergeMapTo } from 'rxjs/operators';
import { take } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs'
@Injectable()
export class MessagingService {

    currentMessage = new BehaviorSubject(null);

    constructor() {
    }

}