import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, Headers, Response, } from '@angular/http';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RiskService {

  constructor(
    private http: HttpClient,
  ) { }
  apiEndpoint = environment.APIEndpoint;

  getList(params, data) {
    return this.http.get(this.apiEndpoint + '/api/v2/risks/applications?product_type=' + params.product_type + '&status=' + data.second + ',' + data.first + '&date_after=' + params.date_after + '&date_before=' + params.date_before + '&search=' + params.q + '&page=' + params.count);
  }

  getRiskFullList(params) {
    return this.http.get(this.apiEndpoint + '/api/v2/risks/applications?product_type=' + params.product_type + '&status=' + params.status + '&date_after=' + params.date_after + '&date_before=' + params.date_before + '&search=' + params.q + '&page=' + params.count);
  }

  readApplication(id) {
    return this.http.get(this.apiEndpoint + '/api/v2/risks/applications/' + id + '/');
  }

  acceptApplication(id, data) {
    return this.http.post(this.apiEndpoint + '/api/v2/risks/applications/' + id + '/accept/', data);
  }

  rejectApplication(id, data) {
    return this.http.post(this.apiEndpoint + '/api/v2/risks/applications/' + id + '/reject/',  data);
  }


}
