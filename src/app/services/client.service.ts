import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, Headers, Response, } from '@angular/http';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { Observable } from 'rxjs';
import * as jwt_decode from 'jwt-decode';
import decode from 'jwt-decode';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router, CanActivate } from '@angular/router';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
import { environment } from '../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';

@Injectable()
export class clientService {
    permList: any;
    apiEndpoint = environment.APIEndpoint;
    constructor(
        private http: HttpClient,
        protected localStorage: LocalStorage,
        public jwtHelper: JwtHelperService,
        public router: Router,
        private spinnerService: Ng4LoadingSpinnerService,
        private toastr: ToastrService

    ) { }

    showError() {
        this.toastr.error('Неверный логин или пароль');
    }

    auth(name, pass) {
        this.spinnerService.show();
        let data;
        data = {
            username: name,
            password: pass
        };
        this.http.post(this.apiEndpoint + '/api/v1/auth/login/', data)
            .subscribe(
                (response: any) => {
                    console.log(response.access);
                    localStorage.setItem('token', response.access);
                    localStorage.setItem('refresh', response.refresh);
                    const tokenInfo = this.getDecodedAccessToken(response.access);
                    console.log(tokenInfo);
                    localStorage.setItem('permissions', tokenInfo.group.name);
                    localStorage.setItem('firstName', response.user.first_name);
                    localStorage.setItem('lastName', response.user.last_name);
                    localStorage.setItem('email', response.user.email);
                    if (tokenInfo.group.name.includes('OP')) {
                      this.router.navigate(['operacionist/home']);
                    } else {
                      this.router.navigate(['manager/request-list']);
                    }
                    this.spinnerService.hide();
                },
                (err: any) => {
                    this.spinnerService.hide();
                    this.showError();
                }
            );
    }

    getDecodedAccessToken(token: string): any {
        try {
            return jwt_decode(token);
        } catch (Error) {
            return null;
        }
    }

    public getToken(): string {
        return localStorage.getItem('token');
    }

    public isAuthenticated(): boolean {
        const token = localStorage.getItem('token');
        return !this.jwtHelper.isTokenExpired(token);
    }

    logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('refresh');
        localStorage.removeItem('permissions');
        this.router.navigate(['']);
    }

    refreshToken() : Observable<any> {
        let token = localStorage.getItem('refresh');
        console.log(token);
      
        return this.http.post<any>(this.apiEndpoint + '/api/v1/auth/update/', { 'refresh': token })
          .pipe(
            map(user => {
                console.log(user);
                console.log(user.access);
              if (user && user.access) {
                localStorage.setItem('token', JSON.stringify(user));
              }
      
              return <any>user;
          }));
      }
}
