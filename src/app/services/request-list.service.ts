import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Http, Headers, Response, } from '@angular/http';
import {LocalStorage} from '@ngx-pwa/local-storage';
import { environment } from '../../environments/environment';
import * as moment from 'moment';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { RequestOptions, ResponseContentType } from '@angular/http';

@Injectable()
export class requestService {


    constructor(
        private http: HttpClient,
        protected localStorage: LocalStorage
    ) {
    }
    permList: any;
    apiEndpoint = environment.APIEndpoint;
Поху;


    initRequest(data) {
      return this.http.post(this.apiEndpoint + '/api/v2/sales/applications/', data);
    }

    getNewRequestInfo(id) {
      return this.http.get(this.apiEndpoint + '/api/v2/sales/enrich/' + id);
    }
    getRequestList() {
        return this.http.get(this.apiEndpoint + '/api/v2/sales/applications/');
    }


    filterListWithParams(params) {
      return this.http.get(this.apiEndpoint + '/api/v2/sales/applications?product_type=' + params.product_type + '&status=' + params.status + '&date_after=' + params.date_after + '&date_before=' + params.date_before + '&search=' + params.q);
  }
    getRequestListWithParams(params) {
        return this.http.get(this.apiEndpoint + '/api/v2/sales/applications?product_type=' + params.product_type + '&status=' + params.status + '&date_after=' + params.date_after + '&date_before=' + params.date_before + '&search=' + params.q + '&page=' + params.count);
    }
    getRequestListWithParamsLA(params) {
      return this.http.get(this.apiEndpoint + '/api/v2/loans/applications?product_type=' + params.product_type + '&status=' + params.status + '&date_after=' + params.date_after + '&date_before=' + params.date_before + '&search=' + params.q + '&page=' + params.count);
  }
    getRequestUsers(){
      return this.http.get(this.apiEndpoint + '/api/v1/user/my_users/');
    }
    getRequest(id) {
        return this.http.get(this.apiEndpoint + '/api/v2/sales/applications/' + id);
    }

    getSignedDocs(id) {
      return this.http.get(this.apiEndpoint + '/api/v2/sales/applications/' + id + '/signed_documents/');
    }
    getLoanRequest(id) {
        return this.http.get(this.apiEndpoint + '/api/v2/loans/applications/' + id);
    }

    getPermList() {
        '';
        return this.http.get('http://' + window.location.hostname + ':4200' + '/assets/perm.json').subscribe(
            data => {
                console.log(data);
                this.permList = data;
            });
    }

    loanAdminList() {
        return this.http.get(this.apiEndpoint + '/api/v2/loans/applications/?status=9');
    }

    loanAdminSoldList() {
      return this.http.get(this.apiEndpoint + '/api/v2/loans/applications/?status=200');
    }

    createRequest(data) {
        return this.http.post(this.apiEndpoint + '/api/v1/catalogs/applications/', data);
    }

    acceptLoan(id) {
        const data = {};
        return this.http.post(this.apiEndpoint + '/api/v2/loans/applications/' + id + '/accept/', data);
    }

    rejectLoan(id, reason) {
        const data = {
          message: reason
        };
        return this.http.post(this.apiEndpoint + '/api/v2/loans/applications/' + id + '/reject/', data);
    }

    geTeamList() {
        return this.http.get(this.apiEndpoint + '/api/v1/user/my_users/');
    }

    getBranchStatistics() {
      return this.http.get(this.apiEndpoint + '/api/v1/sales/applications/branch_statistics?time=2019-06');
    }

    getOffers(id) {
      return this.http.get(this.apiEndpoint + '/api/v2/sales/applications/' + id + '/offers/');
    }
    getLoanOffers(id) {
      return this.http.get(this.apiEndpoint + '/api/v2/loans/applications/' + id + '/offers/');
    }

    getRiskOffer(id) {
      let data = {}
      return this.http.post(this.apiEndpoint + '/api/v2/risks/applications/' + id + '/offers/', data);
    }

    clientReject(id) {
      const data = {};
      return this.http.post(this.apiEndpoint + '/api/v2/sales/applications/' + id + '/client_rejected/', data);
    }

    uploadData(data) {
      return this.http.post(this.apiEndpoint + '/api/v1/sales/applicationdocument/', data);
    }

    selectedStats() {
      return this.http.get(this.apiEndpoint + '/api/v1/sales/applications/selected_month?time=2019-06');
    }

    updateRequest(id, data) {
      return this.http.put(this.apiEndpoint + '/api/v1/sales/enrich/' + id + '/' , data);
    }

    selectOffer(reqId, data) {
      return this.http.post(this.apiEndpoint + '/api/v2/sales/applications/' + reqId + '/select_offer/', data);
    }

    getMessages(id, data) {
      return this.http.post(this.apiEndpoint + '/api/v1/sales/applications/' + id + '/message/', data);
    }

    getHistory(id) {
      return this.http.get(this.apiEndpoint + '/api/v2/sales/applications/' + id + '/history/');
    }

    getLoanHistory(id) {
      return this.http.get(this.apiEndpoint + '/api/v2/loans/applications/' + id + '/history/');
    }


    uploadPhoto(formData) {
          this.http.post(this.apiEndpoint + '/api/v1/sales/applicationdocument/', formData, {
              reportProgress: true,
              observe: 'events'
          });
      }

    getStats():Observable<Blob>{
        let headers = new HttpHeaders();
        headers.append('Content-Type', 'vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        let options = { headers: headers };

        return this.http.get(this.apiEndpoint + '/api/v2/sales/application_overall_statistics/', {responseType: 'blob'})
    }

    getXls(data):Observable<Blob>{
      let headers = new HttpHeaders();
      headers.append('Content-Type', 'vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      let options = { headers: headers };

      return this.http.get(this.apiEndpoint + data, {responseType: 'blob'})
    }

    changeName(id, data) {
      return this.http.patch(this.apiEndpoint + '/api/v2/sales/application_universal_bind/' + id + '/' , data)
    }

}
