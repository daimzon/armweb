import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, Headers, Response, } from '@angular/http';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { environment } from '../../environments/environment';


@Injectable()
export class catalogService {
    constructor(
        private http: HttpClient,
        protected localStorage: LocalStorage
    ) { }
    permList: any;
    apiEndpoint = environment.APIEndpoint;


    getCatalog(type) {
        return this.http.get(this.apiEndpoint + '/api/v1/catalogs/' + type);
    }

    getDynamicCatalog(type) {
      return this.http.get(this.apiEndpoint + '/api/v2/catalogs/' + type);
    }

    getSigners(id, type) {
        return this.http.get(this.apiEndpoint + '/api/v2/sales/applications/'+ id + '/' + type);
    }

    getLastMonthes() {
      return this.http.get(this.apiEndpoint + '/api/v1/sales/applications/last_monthes/');
    }

    getRegion(id) {
      return this.http.get(this.apiEndpoint + '/api/v1/catalogs/region/?country_id=' + id);
    }

    getArea(id) {
      return this.http.get(this.apiEndpoint + '/api/v1/catalogs/area/?region_id=' + id);
    }

    getDocsList(id) {
      return this.http.get(this.apiEndpoint + '/api/v1/catalogs/producttype/' + id +'/documents/')
    }

    sentDocs(data) {

    }

    getStatusList() {
        return this.http.get(this.apiEndpoint + '/api/v2/sales/application_statuses/')
    }

    getCityList(id) {
      return this.http.get(this.apiEndpoint + '/api/v1/catalogs/locality/?area_id=' + id)
    }


  getGcvpStatus() {
    return this.http.get(this.apiEndpoint + '/api/v2/services')
  }


}
