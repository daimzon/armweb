import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpInterceptor,
    HttpSentEvent,
    HttpHeaderResponse,
    HttpProgressEvent,
    HttpResponse,
    HttpUserEvent,
    HttpErrorResponse
} from '@angular/common/http';
import { clientService } from './client.service';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { catchError, switchMap, filter, take, finalize } from 'rxjs/operators';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    error: any;
    isRefreshingToken = false;
  tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);
    constructor(public auth: clientService) {}

    private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
        if (localStorage.getItem('token')) {
         if (!this.isRefreshingToken) {
           this.isRefreshingToken = true;
           this.tokenSubject.next(null);

           return this.auth.refreshToken()
             .pipe(
               switchMap((user: any) => {
                 if (user) {
                   this.tokenSubject.next(user.access);
                   localStorage.setItem('currentUser', JSON.stringify(user));
                   return next.handle(this.addTokenToRequest(request, user.access));
                 }

                 return <any>this.auth.logout();
               }),
               catchError(err => {
                 return <any>this.auth.logout();
               }),
               finalize(() => {
                 this.isRefreshingToken = false;
               })
             );
         } else {
           this.isRefreshingToken = false;
           return this.tokenSubject
             .pipe(filter(token => token != null),

               take(1),
               switchMap(token => {
               return next.handle(this.addTokenToRequest(request, token));
             }));
         }
        } else {
          this.error = 'Неправильный логин или пароль';
          return throwError(this.error);
        }
       }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent  | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any> | any> {
        return next.handle(this.addTokenToRequest(request, this.auth.getToken()))
        .pipe(
          catchError(err => {
            if (err instanceof HttpErrorResponse) {
              switch ((<HttpErrorResponse>err).status) {
                case 401:
                  return this.handle401Error(request, next);
                default:
                  this.error = err;
                  return throwError(this.error);
              }
            } else {
              return throwError(err);
            }
          }));
    }
    private addTokenToRequest(request: HttpRequest<any>, token: string): HttpRequest<any> {
        return request.clone({ setHeaders: { Authorization: `Bearer ${token}`}});
      }


}
