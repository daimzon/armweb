import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, Headers, Response, } from '@angular/http';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { environment } from '../../environments/environment';


@Injectable()
export class NewRequestService {
  constructor(
    private http: HttpClient,
    protected localStorage: LocalStorage
  ) { }
  permList: any;
  apiEndpoint = environment.APIEndpoint;


  contactUpdate(id, data) {
    return this.http.put(this.apiEndpoint + '/api/v2/sales/contact_info/' + id + '/', data);
  }
  getClientAccounts(id) {
    return this.http.get(this.apiEndpoint + '/api/v2/sales/applications/' + id + '/accounts/');
  }
  contactPersonUpdate(id, data) {
    return this.http.post(this.apiEndpoint + '/api/v2/sales/contact_info/' + id + '/contact_person/', data);
  }

  employmentInformationUpdate(id, data) {
    return this.http.put(this.apiEndpoint + '/api/v2/sales/employment_information/' + id + '/', data);
  }

  additionalEmploymentInformationUpdate(id, data) {
    return this.http.put(this.apiEndpoint + '/api/v2/sales/additional_employment/' + id + '/', data);
  }

  identityCardUpdate(id, data) {
    return this.http.put(this.apiEndpoint + '/api/v2/sales/identity_card/' + id + '/', data);
  }

  metaInformationUpdate(id, data) {
    return this.http.put(this.apiEndpoint + '/api/v2/sales/meta_information/' + id + '/', data);
  }

  personUpdate(id, data) {
    return this.http.put(this.apiEndpoint + '/api/v2/sales/person/' + id + '/', data);
  }

  registrationAddressUpdate(id, data) {
    return this.http.put(this.apiEndpoint + '/api/v2/sales/registration_address/' + id + '/', data);
  }

  residentalAddressUpdate(id, data) {
    return this.http.put(this.apiEndpoint + '/api/v2/sales/residental_address/' + id + '/', data);
  }

  spouseUpdate(id, data) {
    return this.http.put(this.apiEndpoint + '/api/v2/sales/spouse/' + id + '/', data);
  }

  getRequest(id) {
    return this.http.get(this.apiEndpoint + '/api/v2/sales/applications/' + id);
  }

  sentDocs(id, data) {
    return this.http.put(this.apiEndpoint + '/api/v2/sales/application_documents_bind/' + id + '/', data);
  }

  sentSignedDocs(id, data) {
    return this.http.put(this.apiEndpoint + '/api/v2/sales/application_signed_documents_bind/' + id + '/', data);
  }

  sentFirstSignedDocs(id, data) {
    return this.http.put(this.apiEndpoint + '/api/v2/sales/application_first_documents_bind/' + id + '/', data);
  }

  createRiskOffer(data) {
    return this.http.post(this.apiEndpoint + '/api/v2/risks/offers/', data);
  }

  riskOfferDelete(id) {
    return this.http.delete(this.apiEndpoint + '/api/v2/risks/offers/' + id + '/');
  }

  riskOfferAdd(id) {
    let data = {}
    return this.http.post(this.apiEndpoint + '/api/v2/risks/applications/' + id + '/offers/', data);
  }

  changeUser(id, data) {
    return this.http.post(this.apiEndpoint + '/api/v2/sales/applications/' + id + '/change_user/', data)
  }

}
