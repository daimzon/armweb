import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { clientService } from './client.service';
@Injectable()
export class AuthGuardService implements CanActivate {
    menu;
    constructor(public auth: clientService, public router: Router) {}
    canActivate(): boolean {
        if (!this.auth.isAuthenticated()) {
            this.router.navigate(['']);
            this.menu = false
            return false;
        } else {
            this.menu = true
            return true;
        }
    }
}
