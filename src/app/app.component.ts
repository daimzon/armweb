import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {requestService} from './services/request-list.service';
import {MessagingService} from "./services/messaging.service";
import {clientService} from './services/client.service';
import {AuthGuardService} from './services/auth-guard.service';
import { LocalStorage } from '@ngx-pwa/local-storage';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'Конвейер';
    breakpoint: number;

    opened: boolean;
    message;
    menu;
    language;

    constructor(
        private translate: TranslateService,
        public perm: requestService,
        private messagingService: MessagingService,
        public auth: clientService,
        public guard: AuthGuardService,
    ) {
        translate.setDefaultLang('ru');
        localStorage.setItem('language', 'ru')
        this.language = localStorage.getItem('language')
    }

    ngOnInit() {
        this.breakpoint = (window.innerWidth <= 400) ? 1 : 6;
        const userId = 'user001';
        this.message = this.messagingService.currentMessage;
        this.checkAuth();
    }

    checkAuth() {
        this.menu = this.auth.isAuthenticated()
    }

    logout() {
        this.auth.logout();
        this.opened = false
    }


    useLanguage(language: string, event) {
        this.translate.use(language);
        console.log(language);
        localStorage.setItem('language', language)
        this.language = language
    }

    onResize(event) {
        this.breakpoint = (event.target.innerWidth <= 400) ? 1 : 6;
    }
}
