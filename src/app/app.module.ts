import {BrowserModule} from '@angular/platform-browser';
import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {Ng4LoadingSpinnerModule} from 'ng4-loading-spinner';
import {MatBadgeModule} from '@angular/material/badge';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {clientService} from './services/client.service';
import {ToastrModule} from 'ngx-toastr';
import {catalogService} from './services/catalogs.service';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './components/login/login.component';
import {RequestListComponent} from './components/manager/request-list/request-list.component';
import {RequestViewComponent, gcvpView} from './components/manager/request-view/request-view.component';
import {NewRequestComponent} from './components/manager/new-request/new-request.component';
import {NewProfileComponent} from './components/manager/new-profile/new-profile.component';
import {OtpCheckComponent} from './components/manager/otp-check/otp-check.component';
import {ClientPhotoComponent} from './components/manager/client-photo/client-photo.component';
import {RequestDocsComponent} from './components/manager/request-docs/request-docs.component';
import {CreditCalcComponent} from './components/manager/credit-calc/credit-calc.component';
// import {FeedbackComponent} from './components/manager/feedback/feedback.component';
import {NewPasswordComponent} from './components/new-password/new-password.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MenuComponent} from './components/menu/menu.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxMaskModule} from 'ngx-mask'
import {OnlyNumber} from './utils/numInput.pipe';
import {JwtHelperService, JwtModule} from '@auth0/angular-jwt';
import {AuthGuardService} from './services/auth-guard.service';
import { DigitOnlyModule } from '@uiowa/digit-only';
import { NewRequestService } from './services/new-request.service';
import { SatDatepickerModule, SatNativeDateModule,  } from 'saturn-datepicker';
import {DoneRequestListComponent} from './components/manager/doneRequest-list/doneRequest-list.component'
import { WebCamModule } from 'ack-angular-webcam';


//Services
import {requestService} from './services/request-list.service';
import {iinService} from './services/iin-check.servise';

//Material
import {MatGridListModule, MatRadioModule, MatIconModule} from '@angular/material';
import {MatSidenavModule} from '@angular/material';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatChipsModule} from '@angular/material/chips';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {NgxQRCodeModule} from 'ngx-qrcode2';
import {MessagingService} from './services/messaging.service';
import {environment} from '../environments/environment';
import {AsyncPipe} from '../../node_modules/@angular/common';
import {WavesModule, BaseChartDirective} from 'angular-bootstrap-md';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import {MatTabsModule} from '@angular/material/tabs';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {TokenInterceptor} from './services/token.interceptor';
import {ChartsModule} from 'ng2-charts';
import {ScrollingModule} from '@angular/cdk/scrolling';
import { ModalModule } from 'ngx-bootstrap';

// In your App's module:
//translate
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {RequestProgressComponent} from './components/manager/request-progress/request-progress.component';
import {NotificationsComponent} from './components/manager/notifications/notifications.component';
import {HomeComponent} from './components/manager/home/home.component';
import {RequestCommentComponent} from './components/manager/request-comment/request-comment.component';
import {RequestProcessComponent} from './components/manager/request-process/request-process.component';
import {RequestAssignComponent} from './components/manager/request-assign/request-assign.component';
import {StatsComponent} from './components/manager/stats/stats.component';
import {NewPhotoComponent} from './components/manager/new-photo/new-photo.component';
import {ChangeManagerComponent} from './components/manager/change-manager/change-manager.component';
// import { OperHomeComponent } from './components/operacionist/oper-home/oper-home.component';
// import { OperIdentificationComponent } from './components/operacionist/oper-identification/oper-identification.component';
import {MatMenuModule} from '@angular/material/menu';
import { StorageModule } from '@ngx-pwa/local-storage';
import { FeedbackFormComponent } from './components/manager/feedback-form/feedback-form.component';
import { RiskListComponent } from './components/manager/risk-list/risk-list.component';
// import { OperClientViewComponent } from './components/operacionist/oper-client-view/oper-client-view.component';
import {MatExpansionModule} from '@angular/material/expansion';


@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        RequestListComponent,
        RequestViewComponent,
        NewRequestComponent,
        NewProfileComponent,
        OtpCheckComponent,
        ClientPhotoComponent,
        RequestDocsComponent,
        CreditCalcComponent,
        // FeedbackComponent,
        NewPasswordComponent,
        DoneRequestListComponent,
        MenuComponent,
        RequestProgressComponent,
        OnlyNumber,
        NotificationsComponent,
        HomeComponent,
        RequestCommentComponent,
        RequestProcessComponent,
        RequestAssignComponent,
        StatsComponent,
        NewPhotoComponent,
        ChangeManagerComponent,
        gcvpView,
        FeedbackFormComponent,
        RiskListComponent,
        // OperHomeComponent,
        // OperIdentificationComponent,
        // OperClientViewComponent
    ],
    imports: [
        MatMenuModule,
        ChartsModule,
        DigitOnlyModule,
        BrowserModule,
        MatBadgeModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatExpansionModule,
        MatGridListModule,
        MatSidenavModule,
        ModalModule.forRoot(),
        NgbModule,
        MatInputModule,
        MatButtonModule,
        MatCardModule,
        MatSelectModule,
        MatIconModule,
        MatDatepickerModule,
        MatNativeDateModule,
        WebCamModule,
        HttpClientModule,
        MatStepperModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        FormsModule,
        ReactiveFormsModule,
        MatDialogModule,
        Ng4LoadingSpinnerModule.forRoot(),
        MatButtonToggleModule,
        MatProgressSpinnerModule,
        MatSnackBarModule,
        NgxMaskModule.forRoot(),
        MatChipsModule,
        MatProgressBarModule,
        MatAutocompleteModule,
        MatCheckboxModule,
        NgxQRCodeModule,
        WavesModule.forRoot(),
        MDBBootstrapModule.forRoot(),
        MatTabsModule,
        ToastrModule.forRoot({
            positionClass: 'toast-bottom-right',
        }),
        JwtModule.forRoot({
            config: {
                tokenGetter: jwtTokenGetter
            }
        }),
        MatRadioModule,
        ScrollingModule,
      SatDatepickerModule,
      SatNativeDateModule,
      StorageModule.forRoot({ IDBNoWrap: false })
    ],
    entryComponents: [
      gcvpView,
      FeedbackFormComponent
    ],
    providers: [MatDatepickerModule, AuthGuardService, requestService, iinService, OnlyNumber, MessagingService, catalogService, clientService, NewRequestService,  AsyncPipe, {
        provide: LocationStrategy,
        useClass: HashLocationStrategy
    },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        },
    ],
    bootstrap: [AppComponent],
    schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {
}

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);

}


export function jwtTokenGetter() {
    return localStorage.getItem('token');
}


