import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {RequestListComponent} from './components/manager/request-list/request-list.component';
import {RequestViewComponent} from './components/manager/request-view/request-view.component';
import {NewRequestComponent} from './components/manager/new-request/new-request.component';
import {NewProfileComponent} from './components/manager/new-profile/new-profile.component';
import {OtpCheckComponent} from './components/manager/otp-check/otp-check.component';
import {ClientPhotoComponent} from './components/manager/client-photo/client-photo.component';
import {RequestDocsComponent} from './components/manager/request-docs/request-docs.component';
import {RequestProgressComponent} from './components/manager/request-progress/request-progress.component';
import {CreditCalcComponent} from './components/manager/credit-calc/credit-calc.component';
import {NewPhotoComponent} from './components/manager/new-photo/new-photo.component';
import {RequestCommentComponent} from './components/manager/request-comment/request-comment.component';
import {RequestAssignComponent} from './components/manager/request-assign/request-assign.component';
import {RequestProcessComponent} from './components/manager/request-process/request-process.component';
import {NewPasswordComponent} from './components/new-password/new-password.component';
import {NotificationsComponent} from './components/manager/notifications/notifications.component';
import {ChangeManagerComponent} from './components/manager/change-manager/change-manager.component';
import {HomeComponent} from './components/manager/home/home.component';
import {StatsComponent} from './components/manager/stats/stats.component';
import {AuthGuardService as AuthGuard} from './services/auth-guard.service';
import {DoneRequestListComponent} from './components/manager/doneRequest-list/doneRequest-list.component'
import { RiskListComponent } from './components/manager/risk-list/risk-list.component';

// import { OperHomeComponent } from './components/operacionist/oper-home/oper-home.component';
// import { OperIdentificationComponent } from './components/operacionist/oper-identification/oper-identification.component';
// import { OperClientViewComponent } from './components/operacionist/oper-client-view/oper-client-view.component';

const routes: Routes = [
    {path: '', component: LoginComponent},
    {path: 'new-password', component: NewPasswordComponent},
    {path: 'manager/request-list', component: RequestListComponent, canActivate: [AuthGuard]},
    {path: 'manager/request-view/:id', component: RequestViewComponent, canActivate: [AuthGuard]},
    {path: 'manager/new-request', component: NewRequestComponent, canActivate: [AuthGuard]},
    {path: 'manager/new-profile/:id', component: NewProfileComponent, canActivate: [AuthGuard]},
    {path: 'manager/otp-check', component: OtpCheckComponent, canActivate: [AuthGuard]},
    {path: 'manager/client-photo', component: ClientPhotoComponent, canActivate: [AuthGuard]},
    {path: 'manager/request-docs', component: RequestDocsComponent, canActivate: [AuthGuard]},
    {path: 'manager/request-progress', component: RequestProgressComponent, canActivate: [AuthGuard]},
    {path: 'manager/credit-calc', component: CreditCalcComponent, canActivate: [AuthGuard]},
    {path: 'manager/change-manager', component: ChangeManagerComponent, canActivate: [AuthGuard]},
    {path: 'manager/new-photo', component: NewPhotoComponent, canActivate: [AuthGuard]},
    {path: 'manager/request-comment', component: RequestCommentComponent, canActivate: [AuthGuard]},
    {path: 'manager/request-assign', component: RequestAssignComponent, canActivate: [AuthGuard]},
    {path: 'manager/request-process', component: RequestProcessComponent, canActivate: [AuthGuard]},
    {path: 'manager/notifications', component: NotificationsComponent, canActivate: [AuthGuard]},
    {path: 'manager/home', component: HomeComponent, canActivate: [AuthGuard]},
    {path: 'manager/stats', component: StatsComponent, canActivate: [AuthGuard]},
    {path: 'manager/done-requests', component: DoneRequestListComponent, canActivate: [AuthGuard]},
    {path: 'manager/risk-requests', component: RiskListComponent, canActivate: [AuthGuard]},
    // {path: 'operacionist/home', component: OperHomeComponent},
    // {path: 'operacionist/identification', component: OperIdentificationComponent},
    // {path: 'operacionist/client/:id', component: OperClientViewComponent},

];


@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
