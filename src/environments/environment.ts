// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export const environment = {
  production: false,
  APIEndpoint: 'https://apiarm.bankrbk.kz:30500',
  firebase: {
    apiKey: "AIzaSyB1LgxhfzohKzEPCiRdOgQUJP5J-li7KS8",
    authDomain: "baldr-aaf2a.firebaseapp.com",
    databaseURL: "https://baldr-aaf2a.firebaseio.com",
    projectId: "baldr-aaf2a",
    storageBucket: "baldr-aaf2a.appspot.com",
    messagingSenderId: "1062087530560"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
